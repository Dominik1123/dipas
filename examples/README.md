# DiPAS - Examples

This folder contains various examples for illustrating the usage of the DiPAS package. More information
can be found in the README files of the respective example folders. Currently the following examples are
available:

* [Inverse Modeling of Quadrupole Gradient Errors via Orbit Response Matrix](./inverse_modeling_via_orm)
* [Quadrupole Tuning at a Transfer Line with Multiple Optimization Targets, such as Beam Spot Size and Beam Loss](./transfer_line_quadrupole_tuning)
* [Tune Matching](./tune_matching)
* [Using External Optimizers](./external_optimizers)
