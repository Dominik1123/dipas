{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Using external optimizers\n",
    "\n",
    "The PyTorch package ships already with a [number of optimizers](https://pytorch.org/docs/stable/optim.html). These however are typically suited for problems with large parameter spaces (several hundred or more). For smaller problems other optimizers, e.g. the [Levenberg-Marquardt algorithm](https://en.wikipedia.org/wiki/Levenberg%E2%80%93Marquardt_algorithm), are more efficient. The Scipy package offers various other optimizers at their [`scipy.optimize.least_squares`](https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html) function. This notebook shows how to use such external optimizers together with the DiPAS framework.\n",
    "\n",
    "In PyTorch the job of the optimizer is to update the parameters of the simulation, given their gradients with respect to some cost function. That is it performs the following steps:\n",
    "\n",
    "1. Compute the new parameter values (based on the current values and gradients),\n",
    "2. Apply these new values, i.e. make the changes effective.\n",
    "\n",
    "We use the external optimizer to perform step (1) and then we apply the changes manually (which is easy)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setup\n",
    "\n",
    "In this example we will estimate quadrupole gradient errors based on ORM matching. In the following we load the lattice, set some random errors and compute the reference ORM."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dipas.build import from_file\n",
    "import dipas.compute as compute\n",
    "from dipas.elements import elements, Marker, Quadrupole, VKicker, VMonitor, tensor\n",
    "import numpy as np\n",
    "import torch\n",
    "\n",
    "\n",
    "elements['multipole'] = lambda **kwargs: Marker()  # all multipoles are turned off\n",
    "\n",
    "lattice = from_file('example.madx')\n",
    "quadrupoles = lattice[Quadrupole]\n",
    "\n",
    "k1 = np.array([q.k1.numpy() for q in quadrupoles])\n",
    "errors = np.random.normal(scale=0.01*abs(k1), size=len(quadrupoles))\n",
    "\n",
    "for q, dk1 in zip(quadrupoles, errors):\n",
    "    q.dk1 = tensor(dk1)\n",
    "    q.update_transfer_map()\n",
    "    \n",
    "orm_ref = compute.orm(lattice, kickers=VKicker, monitors=VMonitor, order=1)[1]  # vertical response only"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's recall the signature of [`least_squares`](https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html). It expects a function `func` which receives N parameters and outputs M residuals, both as Numpy arrays. The default `\"lm\"` (Levenberg-Marquardt) solver requires that `N <= M`. In our case the input N will be the current estimate of the quadrupole errors and the output M is the vertical ORM. The lattice contains 36 quadrupoles, 12 vertical correctors and 12 BPMs. So we have `N = 36` and `M = 12**2`.\n",
    "\n",
    "Within that function `func` we need to make the new parameter estimate effective, similar to above, and then compute the ORM and return it as a flattened Numpy array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "# Quadruoles: 36\n",
      "# Correctors: 12\n",
      "# BPMs: 12\n"
     ]
    }
   ],
   "source": [
    "print(f'# Quadruoles: {len(quadrupoles)}')\n",
    "print(f'# Correctors: {len(lattice[VKicker])}')\n",
    "print(f'# BPMs: {len(lattice[VMonitor])}')\n",
    "\n",
    "\n",
    "def orm_func(estimate):\n",
    "    for q, dk1 in zip(quadrupoles, estimate):\n",
    "        q.dk1 = tensor(dk1)\n",
    "        q.update_transfer_map()\n",
    "    orm = compute.orm(lattice, kickers=VKicker, monitors=VMonitor, order=1)[1]\n",
    "    residuals = (orm - orm_ref).numpy().ravel()\n",
    "    return residuals"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above function is sufficient however the algorithm would still need to estimate the Jacobian itself via finite difference approximation. The number of forward passes, i.e. ORM computations, is approximately on the order of the number of parameters, i.e. 37 in our case (1 baseline + 1 offset for each parameter). This means the algorithm would need to compute 37 different ORMs before it can estimate the Jacobian and provide a new parameter estimate. Here we can be more efficient by reusing the computation graph that is built by the simulation and compute all the gradients with a single forward-backward pass. For this we can use [`torch.autograd.functional.jacobian`](https://pytorch.org/docs/stable/autograd.html#torch.autograd.functional.jacobian). In order to do everything at once and also cache the result of the computation we can wrap everything in a dedicated class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "from collections import deque\n",
    "\n",
    "from torch.autograd.functional import jacobian\n",
    "\n",
    "\n",
    "class MatchORM:\n",
    "    def __init__(self, lattice, orm_ref):\n",
    "        self.lattice = lattice\n",
    "        self.orm_ref = orm_ref\n",
    "        self.quadrupoles = lattice[Quadrupole]\n",
    "        self.jac_cache = deque(maxlen=1)\n",
    "        self.orm_cache = deque(maxlen=1)\n",
    "        self.step = 0\n",
    "        \n",
    "    def __call__(self, estimate):\n",
    "        \"\"\"This function gets invoked by least_squares, i.e. Numpy inputs/outputs.\"\"\"\n",
    "        self.step += 1\n",
    "        jac = jacobian(self._compute, torch.from_numpy(estimate))\n",
    "        self.jac_cache.append(jac.numpy())\n",
    "        residuals = self.orm_cache.pop().detach().numpy()\n",
    "        print(f'[{self.step:02d}] mse = {np.mean(residuals**2)}')\n",
    "        return residuals\n",
    "    \n",
    "    def _compute(self, estimate):\n",
    "        \"\"\"This function gets invoked by jacobian, i.e. PyTorch inputs/outputs.\"\"\"\n",
    "        for q, dk1 in zip(self.quadrupoles, estimate):\n",
    "            q.dk1 = dk1\n",
    "            q.update_transfer_map()\n",
    "        orm = compute.orm(lattice, kickers=VKicker, monitors=VMonitor, order=1)[1]\n",
    "        residuals = torch.flatten(orm - self.orm_ref)\n",
    "        self.orm_cache.append(residuals)\n",
    "        return residuals\n",
    "    \n",
    "    def jac(self, estimate):\n",
    "        \"\"\"This function gets invoked by least_squares, i.e. Numpy output is required.\"\"\"\n",
    "        return self.jac_cache.pop()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can invoke the optimizer by passing it an instance of this class:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[01] mse = 2.3596129695680395\n",
      "[02] mse = 2.3596129695680395\n",
      "[03] mse = 2.3596129695680395\n",
      "[04] mse = 0.04447263051378097\n",
      "[05] mse = 0.019456070405029766\n",
      "[06] mse = 6.416326049531289e-06\n",
      "[07] mse = 3.7826057702729345e-09\n",
      "[08] mse = 1.895252659297642e-19\n",
      "[09] mse = 1.427573236173852e-27\n",
      "[10] mse = 8.101062664219795e-28\n",
      " active_mask: array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,\n",
      "       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])\n",
      "        cost: 5.832765118238252e-26\n",
      "         fun: array([ 0.00000000e+00,  5.32907052e-15, -5.32907052e-15,  8.88178420e-15,\n",
      "        1.24344979e-14, -1.50990331e-14, -2.13162821e-14, -2.79221091e-14,\n",
      "        5.32907052e-15,  0.00000000e+00, -4.08562073e-14,  1.77635684e-15,\n",
      "        2.39808173e-14, -7.10542736e-15, -2.48689958e-14, -3.55271368e-15,\n",
      "        2.13162821e-14, -8.88178420e-15, -3.64153152e-14, -1.24344979e-14,\n",
      "       -1.99840144e-14, -5.15143483e-14, -2.22044605e-15,  7.10542736e-15,\n",
      "        3.55271368e-15, -2.22044605e-14,  8.88178420e-15, -1.42108547e-14,\n",
      "       -1.59872116e-14,  1.24344979e-14,  2.30926389e-14,  2.35367281e-14,\n",
      "        7.10542736e-15,  1.73194792e-14,  2.48689958e-14,  4.44089210e-15,\n",
      "       -2.57571742e-14,  1.42108547e-14,  3.55271368e-15, -1.95399252e-14,\n",
      "       -2.13162821e-14,  7.10542736e-15,  3.10862447e-14, -1.77635684e-15,\n",
      "        3.61932706e-14,  6.03961325e-14, -1.33226763e-15, -1.06581410e-14,\n",
      "       -3.55271368e-15,  1.06581410e-14,  1.77635684e-15,  4.79616347e-14,\n",
      "        1.59872116e-14, -2.30926389e-14, -2.66453526e-14,  1.77635684e-15,\n",
      "       -2.66453526e-14, -3.13082893e-14, -2.30926389e-14,  1.73194792e-14,\n",
      "        1.73194792e-14, -2.30926389e-14, -6.21724894e-15,  1.42108547e-14,\n",
      "       -1.24344979e-14, -1.15463195e-14, -5.15143483e-14, -7.10542736e-15,\n",
      "       -1.33226763e-14, -5.32907052e-14, -1.19904087e-14, -3.55271368e-15,\n",
      "       -1.06581410e-14, -1.92068583e-14, -1.77635684e-15, -4.66293670e-14,\n",
      "       -3.19744231e-14, -8.88178420e-15,  2.84217094e-14,  3.55271368e-15,\n",
      "        3.37507799e-14,  3.73034936e-14,  4.08562073e-14,  1.68753900e-14,\n",
      "       -2.17603713e-14,  5.86197757e-14,  1.32671651e-14, -2.84217094e-14,\n",
      "        4.84057239e-14,  3.73034936e-14,  4.88498131e-14,  7.99360578e-15,\n",
      "       -3.73034936e-14,  4.61852778e-14,  1.77635684e-14, -1.77635684e-14,\n",
      "        8.88178420e-15,  2.57571742e-14,  0.00000000e+00,  3.44169138e-14,\n",
      "        3.55271368e-14,  2.08721929e-14, -4.79616347e-14,  0.00000000e+00,\n",
      "       -4.44089210e-14, -3.10862447e-14, -3.55271368e-14, -4.44089210e-15,\n",
      "        1.24344979e-14, -6.03961325e-14, -5.32907052e-15,  1.42108547e-14,\n",
      "       -6.90558721e-14, -6.39488462e-14,  2.66453526e-15, -1.24344979e-14,\n",
      "        4.08562073e-14, -3.55271368e-14, -2.48689958e-14,  2.30926389e-14,\n",
      "       -1.42108547e-14, -9.76996262e-15,  7.10542736e-15, -2.93098879e-14,\n",
      "       -1.95399252e-14, -8.99280650e-15,  5.50670620e-14, -3.55271368e-15,\n",
      "        1.42108547e-14,  2.48689958e-14,  2.48689958e-14,  8.88178420e-15,\n",
      "       -1.24344979e-14,  6.75015599e-14, -5.32907052e-15, -1.42108547e-14,\n",
      "        8.52651283e-14,  6.57252031e-14, -9.99200722e-15,  1.77635684e-15,\n",
      "       -4.75175455e-14,  4.61852778e-14,  4.26325641e-14, -3.73034936e-14])\n",
      "        grad: array([ 3.87809560e-11,  9.41901644e-11,  4.08196496e-11, -2.94468927e-11,\n",
      "       -7.38044359e-11, -3.31068032e-11,  1.07038273e-11,  3.19154575e-11,\n",
      "        1.49221711e-11, -2.63053813e-11, -6.83893696e-11, -3.06306051e-11,\n",
      "        3.21767308e-11,  8.42847027e-11,  3.79789357e-11, -5.96603917e-11,\n",
      "       -1.47527321e-10, -6.44967040e-11,  3.37752664e-11,  8.59025720e-11,\n",
      "        3.78130009e-11, -5.35994856e-11, -1.30448338e-10, -5.51987925e-11,\n",
      "        2.30886293e-11,  5.42990156e-11,  2.14683893e-11, -4.96429512e-11,\n",
      "       -1.13401927e-10, -4.85920750e-11,  3.48510858e-11,  8.64770499e-11,\n",
      "        3.79478486e-11, -4.45879025e-11, -1.11363277e-10, -4.75798380e-11])\n",
      "         jac: array([[  38.68344469,   76.53415205,   33.34548917, ...,   30.03831099,\n",
      "          96.83563101,   47.25805687],\n",
      "       [  39.26902437,   94.05279423,   40.22871482, ...,  -34.32228054,\n",
      "         -89.98246921,  -39.05381579],\n",
      "       [ -48.90590701,  -96.19572536,  -36.84507096, ...,  -21.05573832,\n",
      "         -73.2725364 ,  -37.02738227],\n",
      "       ...,\n",
      "       [ -37.01005316,  -66.87722996,  -24.06253685, ...,  -47.28657934,\n",
      "        -117.95490619,  -50.10215072],\n",
      "       [ -62.12543355, -144.03738216,  -60.89289824, ...,   33.55563152,\n",
      "          62.51086505,   22.10732741],\n",
      "       [  55.86103047,  110.64148798,   42.57719828, ...,   45.47746351,\n",
      "          98.50820674,   43.19884077]])\n",
      "     message: '`xtol` termination condition is satisfied.'\n",
      "        nfev: 8\n",
      "        njev: 7\n",
      "  optimality: 1.4752732096279823e-10\n",
      "      status: 3\n",
      "     success: True\n",
      "           x: array([ 0.00792058,  0.00719378,  0.00116074,  0.00029487,  0.00290322,\n",
      "       -0.0005206 ,  0.00414219,  0.00465046,  0.00059435,  0.00303476,\n",
      "        0.00179031, -0.00035887,  0.00297726,  0.00078953, -0.00053425,\n",
      "       -0.00600123,  0.00011263,  0.00023978, -0.00287374, -0.00282262,\n",
      "       -0.00067364,  0.00067982, -0.00018771,  0.00066961,  0.00207471,\n",
      "       -0.00108232, -0.00017001,  0.00115687,  0.00464513, -0.00012917,\n",
      "        0.00098835,  0.00054944, -0.00027458,  0.00034512, -0.00637738,\n",
      "        0.00036608])\n"
     ]
    }
   ],
   "source": [
    "from scipy.optimize import least_squares\n",
    "\n",
    "\n",
    "match = MatchORM(lattice, orm_ref)\n",
    "\n",
    "result = least_squares(match, jac=match.jac, x0=np.zeros_like(errors), method='lm')\n",
    "print(result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally let's compare the results with the actual errors:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<matplotlib.legend.Legend at 0x7f5b83ae7210>"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAABDQAAAFzCAYAAADBi1kJAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADh0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uMy4xLjEsIGh0dHA6Ly9tYXRwbG90bGliLm9yZy8QZhcZAAAgAElEQVR4nOzde7hcZXX48e8iCYRLuAoKhDQRAnIRAx4QSksttwSFpFrUKFpQEFSoSCv9JVIBsTwPFrwUBZUKSoUKiAqpooSrtFoJCQYFAk2AAEm5RC7hGjCwfn/MDh0OM3OGnL3nnBm+n+eZ5+z97sta++TknTnr7PfdkZlIkiRJkiR1kzWGOgFJkiRJkqTXyoKGJEmSJEnqOhY0JEmSJElS17GgIUmSJEmSuo4FDUmSJEmS1HUsaEiSJEmSpK4zcqgTGA7e8IY35Pjx44c6DUmSJEmSVGfevHl/yMxNG22zoAGMHz+euXPnDnUakiRJkiSpTkTc12ybQ04kSZIkSVLXsaAhSZIkSZK6jgUNSZIkSZLUdZxDQ5IkSZKkNv3xj39kyZIlrFixYqhT6SmjR49m7NixjBo1qu1jLGhIkiRJktSmJUuWMGbMGMaPH09EDHU6PSEzefTRR1myZAkTJkxo+ziHnEiSJEmS1KYVK1awySabWMwoUUSwySabvOa7XixoSJIkSZL0GljMKN/qfE+HtKAREVMi4q6IWBQRMxpsXysiLim23xQR4+u2zSza74qIyXXtx0fE7RFxW0T8ICJGd+ZqJEmSJEmq3ogRI5g0aRI77bQTBx98ME888cSAx5x11llsv/32HHrooR3IsDOGbA6NiBgBnA3sDywBbo6IWZl5R91uRwCPZ+Y2ETEd+BLwgYjYAZgO7AhsAVwTEdsCbwI+DeyQmc9FxKXFft/r1HVJkiRJkl4/xs/4WannW3z6uwfcZ+2112b+/PkAHHbYYZx99tmceOKJLY8555xzuOaaaxg7dmxbeaxcuZKRI4f3tJtDeYfG7sCizLwnM18ALgam9dtnGnBBsXwZsG/U7kOZBlycmc9n5r3AouJ8UCvSrB0RI4F1gP+t+DokSZIkSRoSe+65J0uXLn15/YwzzmC33XZj55135uSTTwbgE5/4BPfccw8HHnggX/3qV3nmmWf42Mc+xu67784uu+zCFVdcAcD3vvc9pk6dyj777MO+++7b9HyLFy9m++235+Mf/zg77rgjBxxwAM899xwAixYtYr/99uNtb3sbu+66K3fffXfT8wzWUBY0tgQeqFtfUrQ13CczVwLLgU2aHZuZS4EzgfuBB4HlmTm7kuwlSZIkSRpCL774Itdeey1Tp04FYPbs2SxcuJA5c+Ywf/585s2bx4033si3vvUttthiC66//nqOP/54TjvtNPbZZx/mzJnD9ddfzwknnMAzzzwDwC233MJll13GL3/5y6bnA1i4cCHHHHMMt99+OxtuuCE/+tGPADj00EM55phjuPXWW/n1r3/N5ptv3vI8gzG87x95jSJiI2p3b0wAngB+GBEfzswLG+x7FHAUwLhx4zqapyRJkiRJq+u5555j0qRJLF26lO233579998fqBU0Zs+ezS677ALA008/zcKFC9l7771fcfzs2bOZNWsWZ555JlB7csv9998PwP7778/GG2/c8nzjxo1jwoQJTJo0CYC3v/3tLF68mKeeeoqlS5fynve8B4DRo0e/prxeq6EsaCwFtqpbH1u0NdpnSTGEZAPg0RbH7gfcm5nLACLix8CfAq8qaGTmucC5AH19fVnC9XRcs7Fai0d/qPEBpyyvMBtJkiRJUiesmkPj2WefZfLkyZx99tl8+tOfJjOZOXMmRx99dMvjM5Mf/ehHbLfddq9ov+mmm1h33XVfsV+j8y1evJi11lrr5fURI0a8POSkWbx28nqthnLIyc3AxIiYEBFrUpu8c1a/fWYBhxXLhwDXZWYW7dOLp6BMACYCc6gNNdkjItYp5trYF1jQgWuRJEmSJKmj1llnHc466yy+/OUvs3LlSiZPnsz555/P008/DcDSpUt55JFHXnXc5MmT+frXv07t12v47W9/2/D87Z5vlTFjxjB27Fguv/xyAJ5//vmXiy6v5TztGrI7NDJzZUQcC1wFjADOz8zbI+JUYG5mzgLOA74fEYuAx6gVPSj2uxS4A1gJHJOZLwI3RcRlwC1F+28p7sKQJEmSJKnX7LLLLuy888784Ac/4CMf+QgLFixgzz33BGC99dbjwgsvZLPNNnvFMZ///Of5zGc+w84778xLL73EhAkT+OlPf/qqcx9wwAENzzdixIim+Xz/+9/n6KOP5qSTTmLUqFH88Ic/bHqe/nm9VrGqIvN61tfXl3Pnzh3qNF4zh5xIkiRJUmctWLCA7bfffqjT6EmNvrcRMS8z+xrtP5RDTiRJkiRJklaLBQ1JkiRJktR1LGhIkiRJkqSuY0FDkiRJkiR1HQsakiRJkiSp61jQkCRJkiRJXceChiRJkiRJXWTEiBFMmjTp5dfpp5/edN/LL7+cO+644+X1k046iWuuuWbQOTzxxBOcc845gz7PYIwc0uiSJEmSJHWzUzYo+XzLB9xl7bXXZv78+W2d7vLLL+eggw5ihx12AODUU08dVHqrrCpofOpTnyrlfKvDOzQkSZIkSeoBM2bMYIcddmDnnXfms5/9LL/+9a+ZNWsWJ5xwApMmTeLuu+/m8MMP57LLLgNg/PjxzJw5k0mTJtHX18ctt9zC5MmT2XrrrfnWt74FwNNPP82+++7Lrrvuylvf+lauuOKKl2PdfffdTJo0iRNOOAGAM844g912242dd96Zk08+ufLr9Q4NSZIkSZK6yHPPPcekSZNeXp85cyb77bcfP/nJT7jzzjuJCJ544gk23HBDpk6dykEHHcQhhxzS8Fzjxo1j/vz5HH/88Rx++OH86le/YsWKFey000584hOfYPTo0fzkJz9h/fXX5w9/+AN77LEHU6dO5fTTT+e22257+U6R2bNns3DhQubMmUNmMnXqVG688Ub23nvvyr4PFjQkSZIkSeoijYacrFy5ktGjR3PEEUdw0EEHcdBBB7V1rqlTpwLw1re+laeffpoxY8YwZswY1lprLZ544gnWXXddPve5z3HjjTeyxhprsHTpUh5++OFXnWf27NnMnj2bXXbZBajd2bFw4UILGpIkSZIkqbmRI0cyZ84crr32Wi677DK+8Y1vcN111w143FprrQXAGmus8fLyqvWVK1dy0UUXsWzZMubNm8eoUaMYP348K1aseNV5MpOZM2dy9NFHl3dRA7CgoZbGz/hZ022LR3+o8YY2JrGRJEmSJJXn6aef5tlnn+Vd73oXe+21F29+85sBGDNmDE899dRqn3f58uVsttlmjBo1iuuvv5777ruv4XknT57M5z//eQ499FDWW289li5dyqhRo9hss80Gd2EtWNCQJEmSJKmL9J9DY8qUKRx33HFMmzaNFStWkJl85StfAWD69Ol8/OMf56yzznp5MtDX4tBDD+Xggw/mrW99K319fbzlLW8BYJNNNmGvvfZip5124sADD+SMM85gwYIF7LnnngCst956XHjhhZUWNCIzKzt5t+jr68u5c+cOdRqvWbO7J8q8c8I7NCRJkiTp/yxYsIDtt99+qNPoSY2+txExLzP7Gu3vY1slSZIkSVLXsaAhSZIkSZK6jgUNSZIkSZLUdSxoSJIkSZL0GjgXZflW53tqQUOSJEmSpDaNHj2aRx991KJGiTKTRx99lNGjR7+m43xsqyRJkiRJbRo7dixLlixh2bJlQ51KTxk9ejRjx459TcdY0JAkSZIkqU2jRo1iwoQJQ52GcMiJJEmSJEnqQhY0JEmSJElS17GgIUmSJEmSuo4FDUmSJEmS1HUsaEiSJEmSpK5jQUOSJEmSJHUdCxqSJEmSJKnrWNCQJEmSJEldx4KGJEmSJEnqOhY0JEmSJElS17GgIUmSJEmSus6QFjQiYkpE3BURiyJiRoPta0XEJcX2myJifN22mUX7XRExua59w4i4LCLujIgFEbFnZ65GkiRJkiR1ypAVNCJiBHA2cCCwA/DBiNih325HAI9n5jbAV4EvFcfuAEwHdgSmAOcU5wP4F+AXmfkW4G3AgqqvRZIkSZIkddZQ3qGxO7AoM+/JzBeAi4Fp/faZBlxQLF8G7BsRUbRfnJnPZ+a9wCJg94jYANgbOA8gM1/IzCc6cC2SJEmSJKmDhrKgsSXwQN36kqKt4T6ZuRJYDmzS4tgJwDLguxHx24j4TkSs2yh4RBwVEXMjYu6yZcvKuB5JkiRJktQhvTYp6EhgV+CbmbkL8Azwqrk5ADLz3Mzsy8y+TTfdtJM5SpIkSZKkQRrKgsZSYKu69bFFW8N9ImIksAHwaItjlwBLMvOmov0yagUOSZIkSZLUQ4ayoHEzMDEiJkTEmtQm+ZzVb59ZwGHF8iHAdZmZRfv04ikoE4CJwJzMfAh4ICK2K47ZF7ij6guRJEmSJEmdNXKoAmfmyog4FrgKGAGcn5m3R8SpwNzMnEVtcs/vR8Qi4DFqRQ+K/S6lVqxYCRyTmS8Wp/5b4KKiSHIP8NGOXpgkSZIkSarckBU0ADLzSuDKfm0n1S2vAN7X5NjTgNMatM8H+srNVJIkSZIkDSe9NimoJEmSJEl6HbCgIUmSJEmSuo4FDUmSJEmS1HUsaEiSJEmSpK5jQUOSJEmSJHUdCxqSJEmSJKnrWNCQJEmSJEldx4KGJEmSJEnqOhY0JEmSJElS17GgIUmSJEmSuo4FDUmSJEmS1HUsaEiSJEmSpK5jQUOSJEmSJHUdCxqSJEmSJKnrWNCQJEmSJEldx4KGJEmSJEnqOhY0JEmSJElS1xnZbENEzGrj+Mcy8/Dy0pEkSZIkSRpY04IGsD1wZIvtAZxdbjqSJEmSJEkDa1XQODEzf9nq4Ij4Qsn5SJIkSZIkDajpHBqZeelAB7ezjyRJkiRJUtmaFjQiYkREHB0RX4yIvfpt+8fqU5MkSZIkSWqs1ZCTbwPrAHOAsyLil5n5d8W29wL/VHVykl4/xs/4WcP2xaM/1PygU5ZXlI0kSepFft6Qekurx7bunpkfysyvAe8A1ouIH0fEWtQmBJUkSZIkSRoSrQoaa65ayMyVmXkUMB+4Dliv6sQkSZIkSZKaaVXQmBsRU+obMvNU4LvA+CqTkiRJkiRJaqXVU04+nJm/aND+ncwcVW1akiRJkiRJzbW6QwOoPe2kE4lIkiRJkiS1q2VBIyLGAFd0KBdJkiRJkqS2NC1oRMTmwDXAuZ1LR5IkSZIkaWAjW2z7T+CEzJzVqWQkSZIkSZLa0WrIyePAlp1KRJIkSZIkqV2tChrvBA6MiGOqCh4RUyLirohYFBEzGmxfKyIuKbbfFBHj67bNLNrviojJ/Y4bERG/jYifVpW7JEmSJEkaOk2HnGTmMxExFfh2FYGLp6ecDewPLAFujohZmXlH3W5HAI9n5jYRMR34EvCBiNgBmA7sCGwBXBMR22bmi8VxxwELgPWryF16PRk/42cN2xeP/lDzg05ZXlE2kiRJklTT8iknmfliZh5ZUezdgUWZeU9mvgBcDEzrt8804IJi+TJg34iIov3izHw+M+8FFhXnIyLGAu8GvlNR3pIkSZIkaYi1LGjUi4j1I2LjVa8SYm8JPFC3voRXz9nx8j6ZuRJYDmwywLFfA/4BeKmEHCVJkiRJ0jA0YEEjIo6OiIeA3wHzitfcqhNbHRFxEPBIZs5rY9+jImJuRMxdtmxZB7KTJEmSJEllaecOjc8CO2Xm+MycULzeXELspcBWdetji7aG+0TESGAD4NEWx+4FTI2IxdSGsOwTERc2Cp6Z52ZmX2b2bbrppoO/GkmSJEmS1DHtFDTuBp6tIPbNwMSImBARa1Kb5HNWv31mAYcVy4cA12VmFu3Ti6egTAAmAnMyc2Zmjs3M8cX5rsvMD1eQuyRJkiRJGkJNn3JSZybw64i4CXh+VWNmfnowgTNzZUQcC1wFjADOz8zbI+JUYG5mzgLOA74fEYuAx6gVKSj2uxS4A1gJHFP3hBNJkiRJktTj2ilofBu4Dvg9JU+0mZlXAlf2azupbnkF8L4mx54GnNbi3DcAN5SRpyRJkiRJGl7aKWiMysy/qzwTSZIkSZKkNrUzh8bPiyeCbF7yY1slSZIkSZJWSzt3aHyw+Dqzri2BMp50IkmSJEmS9Jo1LWhExBaZ+b+ZOaGTCUmSJEmSJA2k1R0a3ymGltwA/AL4r8xc2ZGsJEmSJEmSWmha0MjMd0XEaOCdwHuAMyPifmrFjV9k5v2dSVGSJEmSJOmVWs6hUTw29RfFi4iYABwIfCMi3pSZu1efoiRJkiRJ0iu1MynoyzLzXuAc4JyIWLOalCRJkiRJklpr57GtrxIRv8/MF8pORpIkSZIkqR2tnnLy3mabgDdVk44kSZIkSdLAWg05uQS4CMgG20ZXk44kSZIkSdLAWhU0fgecmZm39d8QEftVl5IkSZIkSVJrrebQ+AzwZJNt76kgF0mSJEmSpLY0vUMjM/+zxba51aQjSZIkSZI0sKZ3aETEUQMd3M4+kiRJkiRJZWs1h8aMiPhDi+0BHAecW25KktTdxs/4WdNti0d/qPGGU5ZXlI0kSZLUm1oVNH4JHDzA8VeXmIsk6XXKIpAkSZJeq1ZzaHy0k4lIkiRJkiS1q9UdGpJWg39pliRJkqTqtXpsqyRJkiRJ0rDUsqAREWtExPs7lYwkSZIkSVI7WhY0MvMl4B86lIskSZIkSVJb2hlyck1EfDYitoqIjVe9Ks9MkiRJkiSpiXYmBf1A8fWYurYE3lx+OpIkSZIkSQMbsKCRmRM6kYgkSZIkSVK7BixoRMQo4JPA3kXTDcC3M/OPFeYlSZIkSZLUVDtDTr4JjALOKdY/UrQdWVVSkiRJkiRJrbRT0NgtM99Wt35dRNxaVUKSJEmSJEkDaecpJy9GxNarViLizcCL1aUkSZIkSZLUWjt3aJwAXB8R9wAB/Anw0UqzkiRJkiRJaqFlQSMi1gCeAyYC2xXNd2Xm81UnJkmSJEmS1EzLgkZmvhQRZ2fmLsDvOpSTJEmSJElSS+3MoXFtRPx1RETl2UiSJEmSJLWhnYLG0cAPgecj4smIeCoiniwjeERMiYi7ImJRRMxosH2tiLik2H5TRIyv2zazaL8rIiYXbVtFxPURcUdE3B4Rx5WRpyRJkiRJGl4GmkMjgB0z8/6yA0fECOBsYH9gCXBzRMzKzDvqdjsCeDwzt4mI6cCXgA9ExA7AdGBHYAvgmojYFlgJ/H1m3hIRY4B5EXF1v3NKkiRJkqQuN9AcGhkRPwPeWkHs3YFFmXkPQERcDEwD6osP04BTiuXLgG8URZZpwMXF5KT3RsQiYPfM/G/gwSL3pyJiAbBlv3NKkiRJlRk/42cN2xeP/lDzg05ZXlE2ktS72hlycktE7FZB7C2BB+rWlxRtDffJzJXAcmCTdo4thqfsAtzUKHhEHBURcyNi7rJly1b7IiRJkiRJUue1U9B4B/DfEXF3RPwuIn4fEcP6iScRsR7wI+Azmdlwvo/MPDcz+zKzb9NNN+1sgpIkSZIkaVBaDjkpTK4o9lJgq7r1sUVbo32WRMRIYAPg0VbHRsQoasWMizLzx9WkLkmSJEmShtKAd2hk5n3Uigf7FMvPtnNcG24GJkbEhIhYk9okn7P67TMLOKxYPgS4LjOzaJ9ePAVlAjARmFPMr3EesCAzv1JCjpIkSZIkaRga8A6NiDgZ6AO2A74LjAIuBPYaTODMXBkRxwJXASOA8zPz9og4FZibmbOoFSe+X0z6+Ri1ogfFfpdSm+xzJXBMZr4YEX8GfAT4fUTML0J9LjOvHEyukiRJkiRpeGlnyMl7qE2ueQtAZv5v8UjUQSsKDVf2azupbnkF8L4mx54GnNav7b+AKCM3SRrunEVfkiRJr2ftDB15oRjmkQARsW61KUmSJEmSJLXWTkHj0oj4NrBhRHwcuAb412rTkiRJkiRJam7AISeZeWZE7A88SW0ejZMy8+rKM5MkSZIkSWqinTk0KAoYFjEkSZIkSdKwUMbjVyVJkiRJkjrKgoYkSZIkSeo6AxY0IuK4dtokSZIkSZI6pZ07NA5r0HZ4yXlIkiRJkiS1remkoBHxQeBDwISImFW3aQzwWNWJSZIkSZIkNdPqKSe/Bh4E3gB8ua79KeB3VSYlSZIkSZLUStOCRmbeB9wH7Nm5dCRJkiRJkgbWzqSg742IhRGxPCKejIinIuLJTiQnSZIkSZLUSKshJ6v8M3BwZi6oOhm9fo2f8bOG7YtHf6j5QacsrygbSZIkdZKfBSWtjnaecvKwxQxJkiRJkjSctHOHxtyIuAS4HHh+VWNm/riyrCRJkiRJklpop6CxPvAscEBdWwIWNCRJkiRpCDQbpgMthuo4TEc9ZsCCRmZ+tBOJSJIkSZIktaudp5xsGxHXRsRtxfrOEfGP1acmSZIkSZLUWDuTgv4rMBP4I0Bm/g6YXmVSkiRJkiRJrbQzh8Y6mTknIurbVlaUjyRJlfGxgJIkSb2jnTs0/hARW1ObCJSIOAR4sNKsJEmSJEmSWmjnDo1jgHOBt0TEUuBe4MOVZiVJkiRJktRCO085uQfYLyLWBdbIzKeqT0uSJEnDQS8N1fIxl5LUW5oWNCLiw5l5YUT8Xb92ADLzKxXnJkml8oOsJEmS1Dta3aGxbvF1TCcSkSRJkiQNL710l5Z6T9OCRmZ+u/j6hc6lI0mSJEmSNLBWQ07OanVgZn66/HQkSZIkSZIG1uqxrfOK12hgV2Bh8ZoErFl9apIkSZIkSY21GnJyAUBEfBL4s8xcWax/C/jPzqQnSZIkSZL6c36TNh7bCmwErA88VqyvV7RJkiRJktQVfOJd72mnoHE68NuIuB4IYG/glCqTkqpiFVOSJEmSesOABY3M/G5E/Bx4R9H0/zLzoWrTkiRJrycWnCVJ0mvValLQes8DDwKPA9tGxN5lBI+IKRFxV0QsiogZDbavFRGXFNtviojxddtmFu13RcTkds8pSZIkSZK634B3aETEkcBxwFhgPrAH8N/APoMJHBEjgLOB/YElwM0RMSsz76jb7Qjg8czcJiKmA18CPhAROwDTgR2BLYBrImLb4piBzilJktrkeGNJkjRctXOHxnHAbsB9mfmXwC7AEyXE3h1YlJn3ZOYLwMXAtH77TAMuKJYvA/aNiCjaL87M5zPzXmBRcb52zilJkiRJkrpcOwWNFZm5AmpDQDLzTmC7EmJvCTxQt76kaGu4T/HY2OXAJi2ObeeckiRJkiSpy0Vmtt4h4ifAR4HPUBtm8jgwKjPfNajAEYcAUzLzyGL9I8A7MvPYun1uK/ZZUqzfTW1y0lOA32TmhUX7ecDPi8NanrPu3EcBRwGMGzfu7ffdd99gLkfqOG8DV6e85ska/TnryASX9gGrpxM/z536t+mliVT9eV49vfTz3Gt873zt7NOG57UMBxExLzP7Gm1r5ykn7ykWTyke3boB8IsS8loKbFW3PrZoa7TPkogYWcR+dIBjBzonAJl5LnAuQF9fX+uqjiRJkiRJGlZaFjSKiTtvz8y3AGTmL0uMfTMwMSImUCs6TAf6l6tmAYdRm4T0EOC6zMyImAX8e0R8hdqkoBOBOUC0cU5JkiRJqszi09/dZIt/hZfK1LKgkZkvFo9AHZeZ95cZODNXRsSxwFXACOD8zLw9Ik4F5mbmLOA84PsRsQh4jFqBgmK/S4E7gJXAMZn5IkCjc5aZtyRJkvR60fwXc/CXc0lDbcAhJ8BGwO0RMQd4ZlVjZk4dbPDMvBK4sl/bSXXLK4D3NTn2NOC0ds4pSZKk4ctfmiVJq6OdgsbnK89CkiRJkvS65TAdrY52JgUtc94MSZIkSZKkQRuwoBERTwH9nwKyHJgL/H1m3lNFYpKk4cG/mEiSJGk4amfIydeAJcC/U3uKyHRga+AW4HzgnVUlJ0mSJEmS1MgabewzNTO/nZlPZeaTmXkuMDkzL6E2YagkSZIkSVJHtVPQeDYi3h8RaxSv9wMrim39h6JIkiRJkiRVrp0hJ4cC/wKcQ62A8RvgwxGxNnBshblJasFH3EnDl/OOSJIkVa+dp5zcAxzcZPN/lZuOJEmSJEnSwNoZciJJkiRJkjSstDPkRJIkScOMQ5skSa933qEhSZIkSZK6zoAFjYh4Y0ScFxE/L9Z3iIgjqk9NkiRJkiSpsXbu0PgecBWwRbH+P8BnqkpIkiRJkiRpIO0UNN6QmZcCLwFk5krgxUqzkiRJkiRJaqGdgsYzEbEJkAARsQfONiVJkiRJkoZQO085+TtgFrB1RPwK2BQ4pNKsJEmSJEmSWhiwoJGZt0TEXwDbAQHclZl/rDwzSZIkSZKkJpoWNCLivU02bRsRZOaPK8pJkiRJ0gAWn/7uJlscHS7p9aHVHRoHt9iWgAUNSZIkSZI0JJoWNDLzo51MRJIkSZIkqV0DPuUkIjaIiK9ExNzi9eWI2KATyUmSJEmSJDXSzmNbzweeAt5fvJ4EvltlUpIkSZIkSa2089jWrTPzr+vWvxAR86tKSJIkSZIkaSDt3KHxXET82aqViNgLeK66lCRJkiRJklpr5w6NTwIXFPNmBPAYcFilWUmSJEmSJLUwYEEjM+cDb4uI9Yv1JyvPSpIkSZIkqYV2nnKySUScBdwAXB8R/xIRm1SemSRJkiRJUhPtzKFxMbAM+GvgkGL5kiqTkiRJkiRJaqWdOTQ2z8wv1q3/U0R8oKqEJEmSJEmSBtLOHRqzI2J6RKxRvN4PXFV1YpIkSZIkSc20U9D4OPDvwAvF62Lg6Ih4KiKcIFSSJEmSJHVcO085GdOJRCRJkiRJkto1YEEjIvZu1J6ZN5afjiRJkiRJ0sDamRT0hLrl0cDuwDxgn9UNGhEbU3tSynhgMfD+zHy8wX6HAf9YrP5TZl5QtL8d+B6wNnAlcFxmZkScARxMbWjM3cBHM/OJ1c1TkiRJkiQNTwPOoZGZB9e99gd2Al5VfHiNZgDXZuZE4Npi/RWKosfJwDuoFVFOjoiNis3fpDa3x8TiNaVovxrYKTN3Bv4HmDnIPCVJkiRJ0jDUzjoUbQQAABqYSURBVKSg/S0Bth9k3GnABcXyBcBfNdhnMnB1Zj5W3L1xNTAlIjYH1s/M32RmAv+26vjMnJ2ZK4vjfwOMHWSekiRJkiRpGGpnDo2vA1msrgFMAm4ZZNw3ZuaDxfJDwBsb7LMl8EDd+pKibctiuX97fx+jNqyloYg4CjgKYNy4cW0nLkmSJEmShl47c2jMrVteCfwgM3810EERcQ3wpgabTqxfKea+yAb7rbaIOJFarhc12yczzwXOBejr6ys1viRJkiRJqlbLgkZEjAAOyMxDX+uJM3O/Fud9OCI2z8wHiyEkjzTYbSnwzrr1scANRfvYfu1L6859OHAQsG8xJEWSJEmSJPWYlnNoZOaLwJ9ExJolx50FHFYsHwZc0WCfq4ADImKjYjLQA4CriqEqT0bEHhERwN+sOj4ipgD/AEzNzGdLzlmSJEmSJA0T7Qw5uQf4VUTMAp5Z1ZiZXxlE3NOBSyPiCOA+4P0AEdEHfCIzj8zMxyLii8DNxTGnZuZjxfKn+L/Htv68eAF8A1gLuLpW6+A3mfmJQeQpSZIkSZKGoXYKGncXrzWAMWUEzcxHgX0btM8FjqxbPx84v8l+OzVo36aM/CRJkiRJ0vA2YEEjM7/QiUQkSZIkSZLa1bSgERH/wf89rvVVMnNqJRlJkiRJkiQNoNUdGmcWX99L7fGrFxbrHwQerjIpSZIkSZKkVpoWNDLzlwAR8eXM7Kvb9B8RMbfyzCRJkiRJkppo+djWwroR8eZVKxExAVi3upQkSZIkSZJaa+cpJ8cDN0TEPUAAfwIcXWlWkiRJkiRJLbTzlJNfRMRE4C1F052Z+Xy1aUmSJEmSJDXXzh0aABOB7YDRwNsigsz8t+rSkiRJkiRJam7AgkZEnAy8E9gBuBI4EPgvwIKGJEmSJEkaEu1MCnoIsC/wUGZ+FHgbsEGlWUmSJEmSJLXQTkHjucx8CVgZEesDjwBbVZuWJEmSJElSc+3MoTE3IjYE/hWYBzwN/HelWUmSJHWpxae/u8XW5R3LQ5KkXtfOU04+VSx+KyJ+Aayfmb+rNi1JkqTyNS82WGiQJKnbtDMp6N6N2jLzxmpSkiRJkiRJaq2dIScn1C2PBnanNvRkn0oykiRJkiRJGkA7Q04Orl+PiK2Ar1WWkSRJkiRJ0gDaecpJf0uA7ctORJIkSZIkqV3tzKHxdSCL1TWAScAtVSYlSZIkSZLUSluPba1bXgn8IDN/VVE+kiRJkiRJA2qnoPFDYJti+a7MfL7CfCRJkiRJkgbUdA6NiBgVEV8DHgC+C3wPuCciZhTbJ3UkQ0mSJEmSpH5a3aHxZWAdYHxmPgUQEesDZ0bEN4EpwITqU5QkSZIkSXqlVgWNdwETM3PVhKBk5pMR8UngD8CBVScnSZIkSZLUSKvHtr5UX8xYJTNfBJZl5m+qS0uSJEmSJKm5VgWNOyLib/o3RsSHgQXVpSRJkiRJktRaqyEnxwA/joiPAfOKtj5gbeA9VScmSZIkSZLUTNOCRmYuBd4REfsAOxbNV2bmtR3JTJIkSZIkqYlWd2gAkJnXAdd1IBdJkiRJkqS2tJpDQ5IkSZIkaViyoCFJkiRJkrqOBQ1JkiRJktR1LGhIkiRJkqSuMyQFjYjYOCKujoiFxdeNmux3WLHPwog4rK797RHx+4hYFBFnRUT0O+7vIyIj4g1VX4skSZIkSeq8obpDYwZwbWZOBK4t1l8hIjYGTgbeAewOnFxX+Pgm8HFgYvGaUnfcVsABwP1VXoAkSZIkSRo6Q1XQmAZcUCxfAPxVg30mA1dn5mOZ+ThwNTAlIjYH1s/M32RmAv/W7/ivAv8AZGXZS5IkSZKkITVUBY03ZuaDxfJDwBsb7LMl8EDd+pKibctiuX87ETENWJqZt5aesSRJkiRJGjZGVnXiiLgGeFODTSfWr2RmRsSg76aIiHWAz1EbbtLO/kcBRwGMGzdusOElSZIkSVIHVVbQyMz9mm2LiIcjYvPMfLAYQvJIg92WAu+sWx8L3FC0j+3XvhTYGpgA3FrMEToWuCUids/Mhxrkdy5wLkBfX5/DUyRJkiRJ6iJDNeRkFrDqqSWHAVc02Ocq4ICI2KiYDPQA4KpiqMqTEbFH8XSTvwGuyMzfZ+ZmmTk+M8dTG4qya6NihiRJkiRJ6m5DVdA4Hdg/IhYC+xXrRERfRHwHIDMfA74I3Fy8Ti3aAD4FfAdYBNwN/Lyz6UuSJEmSpKFU2ZCTVjLzUWDfBu1zgSPr1s8Hzm+y304DxBg/6EQlSZIkSdKwNFR3aEiSJEmSJK02CxqSJEmSJKnrWNCQJEmSJEldx4KGJEmSJEnqOhY0JEmSJElS17GgIUmSJEmSuo4FDUmSJEmS1HUsaEiSJEmSpK5jQUOSJEmSJHUdCxqSJEmSJKnrWNCQJEmSJEldx4KGJEmSJEnqOhY0JEmSJElS17GgIUmSJEmSuo4FDUmSJEmS1HUsaEiSJEmSpK5jQUOSJEmSJHUdCxqSJEmSJKnrWNCQJEmSJEldx4KGJEmSJEnqOhY0JEmSJElS17GgIUmSJEmSuo4FDUmSJEmS1HUsaEiSJEmSpK5jQUOSJEmSJHUdCxqSJEmSJKnrWNCQJEmSJEldx4KGJEmSJEnqOhY0JEmSJElS17GgIUmSJEmSuo4FDUmSJEmS1HUsaEiSJEmSpK5jQUOSJEmSJHWdISloRMTGEXF1RCwsvm7UZL/Din0WRsRhde1vj4jfR8SiiDgrIqJu299GxJ0RcXtE/HMnrkeSJEmSJHXWUN2hMQO4NjMnAtcW668QERsDJwPvAHYHTq4rfHwT+DgwsXhNKY75S2Aa8LbM3BE4s+LrkCRJkiRJQ2CoChrTgAuK5QuAv2qwz2Tg6sx8LDMfB64GpkTE5sD6mfmbzEzg3+qO/yRwemY+D5CZj1R5EZIkSZIkaWiMHKK4b8zMB4vlh4A3NthnS+CBuvUlRduWxXL/doBtgT+PiNOAFcBnM/PmRglExFHAUQDjxo1bzcuQJGloLD793S22Lu9YHpIkSUOlsoJGRFwDvKnBphPrVzIzIyJLCjsS2BjYA9gNuDQi3lzcyfEKmXkucC5AX19fWfElSZIkSVIHVFbQyMz9mm2LiIcjYvPMfLAYQtJoaMhS4J1162OBG4r2sf3alxbLS4AfFwWMORHxEvAGYNnqXockSZIkSRp+hmoOjVnAqqeWHAZc0WCfq4ADImKjYjLQA4CriqEqT0bEHsXTTf6m7vjLgb8EiIhtgTWBP1R3GZIkSZIkaSgMVUHjdGD/iFgI7FesExF9EfEdgMx8DPgicHPxOrVoA/gU8B1gEXA38POi/XzgzRFxG3AxcFij4SaSJEmSJKm7DcmkoJn5KLBvg/a5wJF16+dTK1I02m+nBu0vAB8uNVlJkiRJkjTsDNUdGpIkSZIkSavNgoYkSZIkSeo6QzLkRJIkSZKkXrT49He32Lq8Y3m8HniHhiRJkiRJ6joWNCRJkiRJUtexoCFJkiRJkrqOBQ1JkiRJktR1LGhIkiRJkqSuY0FDkiRJkiR1HQsakiRJkiSp61jQkCRJkiRJXceChiRJkiRJ6joWNCRJkiRJUtexoCFJkiRJkrqOBQ1JkiRJktR1LGhIkiRJkqSuY0FDkiRJkiR1ncjMoc5hyEXEMuC+oc6jRG8A/tAjcXrpWnotTi9dS6/F6aVr6VScXrqWXovTS9fSqTi9dC29FqeXrqVTcXrpWnotTi9dS6/F6dS1dMqfZOamjTZY0OhBETE3M/t6IU4vXUuvxemla+m1OL10LZ2K00vX0mtxeulaOhWnl66l1+L00rV0Kk4vXUuvxemla+m1OJ26luHAISeSJEmSJKnrWNCQJEmSJEldx4JGbzq3h+L00rX0WpxeupZei9NL19KpOL10Lb0Wp5eupVNxeulaei1OL11Lp+L00rX0WpxeupZei9OpaxlyzqEhSZIkSZK6jndoSJIkSZKkrmNBo8dExJSIuCsiFkXEjIpinB8Rj0TEbVWcv4ixVURcHxF3RMTtEXFcRXFGR8SciLi1iPOFKuIUsUZExG8j4qcVxlgcEb+PiPkRMbfCOBtGxGURcWdELIiIPSuIsV1xHateT0bEZyqIc3zxb39bRPwgIkaXHaOIc1wR4/Yyr6PR/8eI2Dgiro6IhcXXjSqK877iel6KiEHPpN0kxhnFz9nvIuInEbFhRXG+WMSYHxGzI2KLKuLUbfv7iMiIeEMVcSLilIhYWvf/511lxyja/7b497k9Iv55MDGaxYmIS+quY3FEzK8ozqSI+M2q/jMidq8gxtsi4r+Lfvo/ImL9wcQoztnw/bLsfqBFnNL6gRYxSu0HWsQptR9oFqdu+6D7gRbXUnYf0PRayuwHWlxPqf1Aizil9QMtYpTaD0STz7IRMSEibora7wSXRMSaFcU5tohR1ntaszgXRe13nNui1r+OqiDGeUXb76L2OXe9Kq6lbvtZEfH0YGK0ihMR34uIe+v+70wabKxhKTN99cgLGAHcDbwZWBO4Fdihgjh7A7sCt1V4LZsDuxbLY4D/qehaAlivWB4F3ATsUdE1/R3w78BPK/y+LQbeUNX56+JcABxZLK8JbFhxvBHAQ9SeQV3mebcE7gXWLtYvBQ6vIP+dgNuAdYCRwDXANiWd+1X/H4F/BmYUyzOAL1UUZ3tgO+AGoK+iGAcAI4vlL1V4LevXLX8a+FYVcYr2rYCrgPvK+P/a5HpOAT5bxs9Yixh/Wfwsr1Wsb1bV96xu+5eBkyq6ntnAgcXyu4AbKohxM/AXxfLHgC+WcC0N3y/L7gdaxCmtH2gRo9R+oEWcUvuBZnGK9VL6gRbXUnYf0CxOqf1Aq+9Z3T6D7gdaXE9p/UCLGKX2AzT5LEvtM830ov1bwCcrirMLMJ6SPoO2iPOuYlsAPxjM9bSIUd8HfIWiDy07TrHeB3wfeLrC79n3gEMGe/7h/vIOjd6yO7AoM+/JzBeAi4FpZQfJzBuBx8o+b78YD2bmLcXyU8ACar98lh0nM3NVZXRU8Sp9YpmIGAu8G/hO2efutIjYgNoH9fMAMvOFzHyi4rD7Andn5n0VnHsksHZEjKRWcPjfCmJsD9yUmc9m5krgl8B7yzhxk/+P06gVnSi+/lUVcTJzQWbeNdhzDxBjdvE9A/gNMLaiOE/Wra5LCf1Ai77yq8A/lBFjgDilaRLjk8Dpmfl8sc8jFcUBICICeD+1D7JVxElg1V9KN2CQfUGTGNsCNxbLVwN/PZgYRZxm75el9gPN4pTZD7SIUWo/0CJOqf3AAJ9lSukHOvh5qVmcUvuBga6nrH6gRZzS+oEWMUrtB1p8lt0HuKxoL6MPaBgnM3+bmYsHc+4241xZbEtgDoPoB1rEeBJe/jlbm8H//2wYJyJGAGdQ6wMGrVO/zwxXFjR6y5bAA3XrS6jgTa3TImI8tervTRWdf0Rx6+IjwNWZWUWcr1HrtF6q4Nz1EpgdEfMi4qiKYkwAlgHfjdoQmu9ExLoVxVplOiX8EtNfZi4FzgTuBx4Elmfm7LLjULs7488jYpOIWIfaXxm2qiDOKm/MzAeL5YeAN1YYq5M+Bvy8qpNHxGkR8QBwKHBSRTGmAUsz89Yqzt/PscVts+dHCcOOGtiW2s/1TRHxy4jYrYIY9f4ceDgzF1Z0/s8AZxQ/A2cCMyuIcTv/94eG91FyP9Dv/bKyfqDq9+UBYpTaD/SPU1U/UB+nqn6gwfeskj6gX5zK+oEmPwOl9wP94lTSD/SLUXo/0P+zLLU7tp+oKwSW8jtBhz4zt4xTDDX5CPCLKmJExHep9ZlvAb4+mBgt4hwLzKrrowetxffstKIf+GpErFVWvOHEgoaGtWLs2o+Az/T7y0lpMvPFzJxErdK7e0TsVOb5I+Ig4JHMnFfmeZv4s8zcFTgQOCYi9q4gxkhqt1F/MzN3AZ6hdjtzJYoxn1OBH1Zw7o2ofaiYAGwBrBsRHy47TmYuoHab9Gxqb8DzgRfLjtMkdtIDVfqIOBFYCVxUVYzMPDEztypiHFv2+Yti1ueoqFjSzzeBrYFJ1Ip1X64gxkhgY2q3tZ4AXFr8VasqH6SCwmadTwLHFz8Dx1PchVayjwGfioh51G5Bf6GsE7d6vyyzH+jE+3KzGGX3A43iVNEP1Mehln/p/UCDa6mkD2gQp5J+oMXPWan9QIM4pfcDDWKU3g/0/yxL7Zfx0lX9mbnNOOcAN2bmf1YRIzM/Su0z4QLgA4OJ0STO3tQKWYMulgwQZydqBbm3ALtR+3/6/8qMOVxY0OgtS3lllXds0daVigrsj4CLMvPHVcfL2rCJ64EpJZ96L2BqRCymNgxon4i4sOQYwMt3HKy65fMn1N7UyrYEWFJX+b2MWoGjKgcCt2TmwxWcez/g3sxclpl/BH4M/GkFccjM8zLz7Zm5N/A4tbG0VXk4IjYHKL4OeijAUIqIw4GDgEOLX8yqdhElDAVoYGtqxbNbi/5gLHBLRLyp7ECZ+XDx4eYl4F+pri/4cXGr6xxqd6ANekK4RoohYe8FLqni/IXDqPUBUCuglv49y8w7M/OAzHw7tV/K7i7jvE3eL0vvBzrxvtwsRtn9QBvXUko/0CBO6f1Ao2upog9o8j0rvR9o8TNQaj/QJE6p/UCTf5tK+oHi3Ks+y+4JbFh8z6Dk3wkq/MzcMk5EnAxsSm1eukpiFG0vUvvMXtpngbo4fwlsAywq+oB1ImJRBXGmZG3YU2ZtSNh3qeazwJCzoNFbbgYmRm1W4zWp3aY/a4hzWi1Fdf88YEFmfqXCOJtGMVt6RKwN7A/cWWaMzJyZmWMzczy1f5PrMrP0uwAiYt2IGLNqmdokaqU/iSYzHwIeiIjtiqZ9gTvKjlOnyr/K3g/sERHrFD9z+1KryJcuIjYrvo6j9oHs36uIU5hF7UMZxdcrKoxVqYiYQm241tTMfLbCOBPrVqdRcj8AkJm/z8zNMnN80R8soTZh3ENlx1r1i2zhPVTQFwCXU/tgRkRsS22C4D9UEAdqxcc7M3NJReeH2lj5vyiW9wFKH9pS1w+sAfwjtYn6BnvOZu+XpfYDnXhfbhaj7H6gRZxS+4FGccruB1pcS6l9QIt//1L7gQF+zkrrB1rEKa0faPFvU2o/0OSz7AJqv9QeUuxWRh9Q+WfmVnEi4khgMvDBolBXdoy7ImKboi2o3R082D6gUZx5mfmmuj7g2czcpoI4d9YVtYPaHCqVPaFySOUwmJnUV3kvamPz/4datffEimL8gNrti3+k9kZ8RAUx/oza7bG/o3Z7/nzgXRXE2Rn4bRHnNkqYPX+AeO+koqecUHu6za3F6/aq/v2LWJOAucX37XJgo4rirAs8CmxQ4bV8gf/f3t2F2FWdcRh//n5XbWwrsQ1YaIlBaMo4MCpEFCXtRXtRMEVaKpgqBfGqRSh4YUsVEUQoiqZU6FfSDBSR2mrjhTQphCS0lST1K63WqgEvRKGIxfhF9PVir5HjOHPOkNkHPdPnBwN71ll7vXtt5qyzzzt7rd19YD1Jt9r0yWOKs4cu8fMY8JUe2/3Q+xE4E9hFdyG2E/jMmOJsattvAS8BD48hxn/o1gaaGwf6ePrIQnF+3/4GHgf+RLdAYO9x5r1+mH5WhF+oP9uBJ1p/HgTWjCHGScBsO28HgY3jOmd0K7Vft9z2R/TnYuBAe4/+HZgZQ4wf0H1G/xu4DUgPfVnw87LvcWBInN7GgSExeh0HhsTpdRxYLM68OssaB4b0pe8xYLE4vY4Dw85Zn+PAkP70Ng4MidHrOMAi17J014WPtPfPfSzz+mZInO+3MeAoXULol2OKc5Tu+83cuTzma/aFYtD9o39fe988SXeX1qpx9GVenT6ecrLYOfvLQH9maU9CWWk/aZ2VJEmSJEmaGE45kSRJkiRJE8eEhiRJkiRJmjgmNCRJkiRJ0sQxoSFJkiRJkiaOCQ1JkiRJkjRxTGhIkqReJDk7yQNJnknyXJItSU7uqe3Lkuzoo60RcbYmueIY9jstyc62vTfJCf0fnSRJGmRCQ5IkLVuSAPcDf6yqdcA64BPA7WOO+3FJHGwA/prk08CRqjr6UR+QJEkrnQkNSZLUh43Am1X1G4Cqege4Htic5PQkVyfZMlc5yY4kl7XtnyfZn+RQkpsH6nwtyVNJDgLfHCi/Kcn2JPuA7SPafi3JHa3tXUlWt/LpJH9L8niSP7RExAckmUmyO8mBJA8nWbNAnbVJHgVmgSuBA8B5SR5NctYyzqckSRrBhIYkSerDerov8++rqv8Bh4FzRux7Y1WdD0wBlyaZSnIK8AvgG8AM8Ll5+3wJ+GpVfWdE26cB+6tqPbAb+Ekr/y1wQ1VNAU8MlAOQ5ETgbuCKqpoBfg3cOr/xqnq2qqbp+n4hsA34XlVNV9XLI45NkiQtw8flNk1JkvT/61tJrqW7LllDl6w4Dni+qp4BSDILXDuwz4NV9cYS2n4XuLdtzwL3JzkD+FRV7W7l24D75u13LvBl4M/dbBqOB14cEuesqvpvkingV0s4LkmStEwmNCRJUh/+CXxgMc0kq+jurHiaLjkweGfoKa3OF4EfAhdU1StJts69NsKRge2jC7W9iFpC2wABDlXVhqGVknuAi4Gz29STdcCOJNuq6o4lxpIkScfAKSeSJKkPu4BTk2wGSHI88FNgS7uT4jAwneS4JJ+nm54BsIouOfFqks8CX2/lTwFfSLK2/T5saslibUN3rTOXaLkS2FtVrwKvJLmklV9FNx1l0NPA6iQbWn9OTLJ+fuCqug64GbgFuBx4qE03MZkhSdKYeYeGJElatqqqJJuAnyX5MbAauLeq5tad2Ac8T3cnx7+Ag22/x5L8gy6B8UKrR1W92aahPJTkdWAP8MlFwi/YdnMEuDDJj4CXgW+38u8C9yQ5FXgOuGZef95uj2+9q01ROQG4Ezi0QPxL6dbkuIQPJ0YkSdKYpGqpd15KkiQtTZKLgN8Bm6rq4Kj6YzyO16rq9I8qviRJGh8TGpIkacUyoSFJ0splQkOSJEmSJE0cFwWVJEmSJEkTx4SGJEmSJEmaOCY0JEmSJEnSxDGhIUmSJEmSJo4JDUmSJEmSNHFMaEiSJEmSpInzHrPo6ekWsjH4AAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 1296x432 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "\n",
    "fig, ax = plt.subplots(figsize=(18, 6))\n",
    "ax.set(xlabel='Quadrupole #', ylabel='Quadrupole gradient error [1/m^2]')\n",
    "width = 0.25\n",
    "x = np.arange(len(errors))\n",
    "ax.bar(x - width/2, errors, width, label='Reference')\n",
    "ax.bar(x + width/2, result.x, width, label='Estimate')\n",
    "ax.set_xticks(x)\n",
    "ax.legend()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
