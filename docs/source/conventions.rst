Conventions
===========

Regarding the various conventions we mostly follow MADX in order to provide a smooth transition from one program to the
other and hence the user may refer to [1]_ (Chapter 1) for details.

* Units for the various physical quantities follow the MADX definitions (see [1]_, Chapter 1.8).
* The phase-space coordinates used for particle tracking are ``(x, px, y, py, t, pt)`` (see [2]_ for details).
* A right-handed curvilinear coordinate system is assumed (see [1]_, Chapter 1.1).

Similar analogies with MADX hold for the various commands and their parameters.


.. [1] Hans Grote, Frank Schmidt, Laurent Deniau and Ghislain Roy, "The MAD-X Program (Methodical Accelerator
       Design) Version 5.02.08 - User's Reference Manual, 2016
.. [2] F. Christoph Iselin, "The MAD program (Methodical Accelerator Design) Version 8.13 -
       Physical Methods Manual", 1994
