Welcome to DiPAS' documentation!
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   conventions
   compatibility
   usage/index
   examples/index
   api/modules
   changelog


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
