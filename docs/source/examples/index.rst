Examples
========

The following examples show the setup and implementation of various use cases with the DiPAS package.

The relevant example and code files as well as the complete walkthrough can be found at
`the repository <https://gitlab.com/Dominik1123/dipas/-/tree/master/examples>`__.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   inverse_modeling
   tune_matching
   transfer_line
