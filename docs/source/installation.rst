Installation
============

To satisfy the requirements, please install the latest version of PyTorch according to the instructions on the
`PyTorch website`_.

Then DiPAS can be installed from PyPI: ``pip install dipas``.

On Windows it is recommended to use Anaconda or Miniconda for the Python setup in order to make sure the project's
dependencies are handled correctly.

.. _PyTorch website: https://pytorch.org/
