Usage
=====

The DiPAS package can be used for various tasks, among which are

* parsing MADX scripts,
* building lattices, either from scripts or programmatically,
* (differentiable) particle tracking,
* (differentiable) optics calculations, such as closed orbit search and Twiss computation,
* serializing lattices to MADX scripts,
* run MADX scripts and related tasks,
* visualizing lattices.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   building
   inspecting
   modifying
   converting
   optimizing
   tracking
   optics
   serializing
   utilities
   visualizing
   cli
