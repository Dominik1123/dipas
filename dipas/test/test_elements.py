from functools import partial
import importlib.resources
import itertools as it
import operator as op
import os
import random
import re
import tempfile
import unittest
import warnings

import numpy as np

from dipas.backends import backend, F, Numpy
from dipas.build import from_file, from_script, error_script, sequence_script, track_script, create_script, Beam
from dipas.elements import Element, Drift, Kicker, HKicker, VKicker, Quadrupole, \
    ThinQuadrupole, Sextupole, ThinSextupole, SBend, Monitor, Marker, AlignmentError, LongitudinalRoll, Tilt, Offset, \
    BPMError, Aperture, ApertureCircle, ApertureEllipse, ApertureRectangle, ApertureRectEllipse, Segment, ThinElement, \
    CompoundElement, Utilities, elements, use_drift_for, ElementNotFoundError
from dipas.madx import run_script
from dipas.utils import numpy_compatible


run_script = partial(run_script, madx=os.path.expanduser('~/bin/madx'))
columns = ['X', 'PX', 'Y', 'PY', 'T', 'PT']
e1_label = 'e1'
e1_allclose = partial(np.allclose, atol=1e-12, rtol=1e-9)


def sequence_script_from_list(elements):
    return sequence_script(Segment(elements))


def error_script_from_list(elements):
    return error_script(Segment(elements))


class _TestBase(unittest.TestCase):
    def assertArrayEqual(self, *arrays):
        assert len(arrays) == 2
        arrays = list(arrays)
        for i, x in enumerate(arrays):
            if isinstance(x, backend.TensorType):
                x = backend.to_numpy(x)
            if isinstance(x, np.ndarray):
                x = x.tolist()
            if isinstance(x, tuple):
                x = list(x)
            arrays[i] = x
        self.assertListEqual(*arrays)


class TestElement(unittest.TestCase):
    def test_abc_methods(self):
        with self.assertRaises(NotImplementedError):
            Element(2.0, beam={}).exact(backend.make_zeros(6, 10))

    def test_makethin_deltas(self):
        expected = {2: {'edge': (0.0, 1.0), 'simple': (0.25, 0.50), 'teapot': (1/6, 2/3)},
                    3: {'edge': (0.0, 0.5), 'simple': (1/6, 1/3), 'teapot': (0.125, 0.375)},
                    4: {'edge': (0.0, 1/3), 'simple': (0.125, 0.25), 'teapot': (0.1, 4/15)}}
        for n in range(2, 5):
            for style in ('edge', 'simple', 'teapot'):
                with self.subTest(f'[{style}] n = {n}'):
                    self.assertTupleEqual(ThinElement.makethin_deltas[style](n), expected[n][style])


# noinspection PyUnresolvedReferences
class TestAperture(_TestBase):
    def _check(self, element, x, outside_expected):
        t = backend.from_numpy(x)
        x1 = backend.to_numpy(element.loss(t))
        x2 = backend.to_numpy(element.aperture(t[[0, 2]]))
        self.assertTupleEqual(x1.shape, x.shape[1:])
        self.assertTrue(np.all(x1 == x2))
        self.assertSequenceEqual((x1 > 0).tolist(), outside_expected)

        # Check with MADX.
        beam = dict(beta=0.6, gamma=1.25)
        element = Marker(label=e1_label, aperture=element.aperture)
        track = track_script(x, [e1_label], aperture=True, recloss=True, maxaper=[1e3]*6)
        sequence = sequence_script_from_list([element, Drift(l=1, beam=beam)])
        script = create_script(beam, sequence=sequence, track=track)
        loss = run_script(script, ['trackloss'])['trackloss']
        self.assertTrue(np.array_equal(loss.loc[:, 'NUMBER'], np.arange(1, x.shape[1]+1)[outside_expected]))
        self.assertTrue(np.all(loss.loc[:, 'S'] == 0))

        # Check round-trip via script.
        element = from_script(script, beam=beam)[0]
        self.assertTupleEqual(x1.shape, backend.to_numpy(element.loss(t)).shape)
        self.assertTrue(np.all(x1 == backend.to_numpy(element.loss(t))))

    def test_default(self):
        e = Element(0, beam={})
        self.assertIsNone(e.aperture)

    def test_elliptical(self):
        e = Element(0, beam={}, aperture=ApertureEllipse((3.0, 4.0)))
        x = np.zeros((6, 5), dtype=float)
        x[0] = [1.5, 3.000001, 0, 2.5, 5]
        x[2] = [-3, 0, 4.000001, 3.5, -5]
        self._check(e, x, [False, True, True, True, True])

    def test_rectangular(self):
        e = Element(0, beam={}, aperture=ApertureRectangle((3.0, 4.0)))
        x = np.zeros((6, 5), dtype=float)
        x[0] = [1.5, 3.000001, 0, 2.5, 5]
        x[2] = [-3, 0, 4.000001, 3.5, -5]
        self._check(e, x, [False, True, True, False, True])

    def test_rectangle_ellipse(self):
        e = Element(0, beam={}, aperture=ApertureRectEllipse((8.0, 4.0, 9.0, 5.0)))
        x = np.zeros((6, 6), dtype=float)
        x[0] = [-8.0, 0.0, 7.0, 9.0,  0.0, 7.5]
        x[2] = [ 0.0, 4.0, 0.5, 0.0, -5.0, 3.5]
        self._check(e, x, [False, False, False, True, True, True])

    def test_padding(self):
        e = Element(0, beam={}, aperture=ApertureRectangle(aperture=(3.0, 4.0), padding=(1.0, 1.0)))
        self.assertArrayEqual(e.aperture.aperture, [3.0, 4.0])
        self.assertArrayEqual(e.aperture.padding, [1.0, 1.0])
        x = np.zeros((6, 5), dtype=float)
        x[0] = [1.5, 2.000001, 0, 1.5, 4]
        x[2] = [-2.999, 0, 3.000001, 2.5, -4]
        self._check(e, x, [False, True, True, False, True])

        e = Element(0, beam={}, aperture=ApertureRectangle(aperture=(3.0, 4.0), padding=(1.0, 3.0)))
        self.assertArrayEqual(e.aperture.aperture, [3.0, 4.0])
        self.assertArrayEqual(e.aperture.padding, [1.0, 3.0])
        x = np.zeros((6, 6), dtype=float)
        x[0] = [0.0, 1.5, 2.000001, 0, 1.5, 4]
        x[2] = [0.0, -2.999, 0, 3.000001, 2.5, -4]
        self._check(e, x, [False, True, True, True, True, True])

    def test_aperture_offset(self):
        e = Element(0, beam={}, aperture=ApertureRectangle((2.0, 4.0), offset=(3.0, 1.0)))
        x = np.zeros((6, 5), dtype=float)
        x[0] = [5.1,  1.001, 4.999, 5.0, 2.5]
        x[2] = [5.0, -2.999, 4.999, 5.1, 4.5]
        self._check(e, x, [True, False, False, True, False])

    def test_abc_methods(self):
        with self.assertRaises(NotImplementedError):
            Aperture(1, 0).forward(backend.make_zeros(2, 10))


class TestMarker(unittest.TestCase):
    def test(self):
        m = Marker(l=0, beam=dict(particle='proton'))
        self.assertEqual(m.l, 0.)
        self.assertDictEqual(m.beam, dict(particle='proton'))
        x0 = np.random.uniform(-0.001, 0.001, size=(6, 100))
        x1 = numpy_compatible(m.linear)(x0)
        x2 = numpy_compatible(m.exact)(x0)
        self.assertTrue(np.all(x1 == x0))
        self.assertTrue(np.all(x2 == x0))


class TestDrift(unittest.TestCase):
    def test_exact(self):
        beam_config = {'particle': 'proton', 'beta': 0.6, 'gamma': 1.25}
        x0 = np.random.uniform(-0.001, 0.001, size=(6, 100))
        track = track_script(x0, [e1_label], aperture=False, recloss=False)
        for l in np.logspace(-1, 3, 50):
            with self.subTest(l=l):
                drift = Drift(l, beam=beam_config, label=e1_label)
                sequence = sequence_script_from_list([drift])
                script = create_script(beam_config, sequence=sequence, track=track)
                result = run_script(script, ['trackone'])
                x1_ref = result['trackone'].loc[e1_label, columns].values.T
                x1 = numpy_compatible(drift.exact)(x0)
                self.assertTrue(e1_allclose(x1, x1_ref))


class TestKicker(unittest.TestCase):
    kicker_cls = Kicker

    def test_linear_zero_length(self):
        beam_config = {'particle': 'proton', 'beta': 0.6, 'gamma': 1.25}
        x0 = np.random.uniform(-0.001, 0.001, size=(6, 100))
        track = track_script(x0, [e1_label], aperture=False, recloss=False)
        drift = Drift(0.1, beam=beam_config)  # Need drift to create non-zero length sequence.
        for _kick in np.logspace(-4, -1, 25):
            for sign in (-1, 1):
                kick = _kick * sign
                with self.subTest(kick=kick):
                    config = {list(self.kicker_cls.__annotations__)[0]: kick, 'l': 0, 'beam': beam_config}
                    kicker = self.kicker_cls(**config, label=e1_label)
                    sequence = sequence_script_from_list([drift, kicker])
                    script = create_script(beam_config, sequence=sequence, track=track)
                    result = run_script(script, ['trackone'])
                    x1_ref = result['trackone'].loc[e1_label, columns].values.T
                    x1 = backend.to_numpy(kicker.linear(drift.exact(backend.from_numpy(x0))))
                    self.assertTrue(e1_allclose(x1, x1_ref))


class TestHKicker(TestKicker):
    kicker_cls = HKicker


class TestVKicker(TestKicker):
    kicker_cls = VKicker


class TestQuadrupole(unittest.TestCase):
    def test_linear(self):
        beam_config = {'particle': 'proton', 'beta': 0.6, 'gamma': 1.25}
        x0 = np.random.uniform(-0.001, 0.001, size=(6, 100))
        # MADX considers non-linear effects for path length difference and influence of momentum error:
        # https://github.com/MethodicalAcceleratorDesign/MAD-X/blob/85dea91cb5ebca66963741840aa661cf033d224e/src/trrun.f90#L4390
        # https://github.com/MethodicalAcceleratorDesign/MAD-X/blob/85dea91cb5ebca66963741840aa661cf033d224e/src/trrun.f90#L4346
        x0[[4, 5], :] = 0
        track = track_script(x0, [e1_label], aperture=False, recloss=False)
        for l in np.logspace(-1, 1, 5):
            for _k1 in np.logspace(-5, 0, 10):
                for sign in (-1, 1):
                    k1 = _k1 * sign
                    with self.subTest(l=l, k1=k1):
                        quadrupole = Quadrupole(k1, l, beam=beam_config, label=e1_label)
                        sequence = sequence_script_from_list([quadrupole])
                        script = create_script(beam_config, sequence=sequence, track=track)
                        result = run_script(script, ['trackone'])
                        x1_ref = result['trackone'].loc[e1_label, columns].values.T
                        x1_ref[4, :] = 0  # MADX considers non-linear effects for path length difference (see above).
                        x1 = numpy_compatible(quadrupole.linear)(x0)
                        self.assertTrue(e1_allclose(x1, x1_ref))

    def test_zero_gradient(self):
        beam_config = {'particle': 'proton', 'beta': 0.6, 'gamma': 1.25}
        l = 5.
        x0 = backend.from_numpy(np.random.uniform(-0.001, 0.001, size=(6, 100)))
        self.assertTrue(F.all(Quadrupole(0., l, beam=beam_config).linear(x0) == Drift(l, beam=beam_config).linear(x0)))

    @unittest.skipIf(isinstance(backend, Numpy), 'Numpy does not use gradients')
    def test_k1_as_parameter_cannot_go_to_zero(self):
        x0 = backend.make_zeros(6, 1)
        with self.assertRaises(ValueError):
            Quadrupole(backend.as_parameter(backend.from_numbers(0.)), 5.0, beam={'beta': 0.6, 'gamma': 1.25}).linear(x0)


class TestSextupole(unittest.TestCase):
    def test_linear(self):
        element = Sextupole(np.random.uniform(0.1, 0.5), 2.5, beam=dict(particle='proton', beta=0.6, gamma=1.25))
        x0 = np.random.uniform(-0.001, 0.001, size=(6, 100))
        x1 = numpy_compatible(element.linear)(x0)
        self.assertTrue(np.array_equal(x1, numpy_compatible(Drift(element.l.item(), beam=element.beam).linear)(x0)))


class TestSBend(unittest.TestCase):
    def test_linear(self):
        beam_config = {'particle': 'proton', 'beta': 0.6, 'gamma': 1.25}
        x0 = np.random.uniform(-0.001, 0.001, size=(6, 100))
        # MADX considers non-linear effects for path length difference and influence of momentum error:
        # https://github.com/MethodicalAcceleratorDesign/MAD-X/blob/85dea91cb5ebca66963741840aa661cf033d224e/src/trrun.f90#L4390
        # https://github.com/MethodicalAcceleratorDesign/MAD-X/blob/85dea91cb5ebca66963741840aa661cf033d224e/src/trrun.f90#L4345
        x0[[4, 5], :] = 0
        track = track_script(x0, [e1_label], aperture=False, recloss=False)
        for l in np.logspace(-1, 2, 5):
            for _angle in np.pi/4 * np.logspace(-5, 0, 10):
                for sign in (-1, 1):
                    angle = _angle * sign
                    with self.subTest(l=l, angle=angle):
                        sbend = SBend(angle, l, beam=beam_config, label=e1_label)
                        sequence = sequence_script_from_list([sbend])
                        script = create_script(beam_config, sequence=sequence, track=track)
                        result = run_script(script, ['trackone'])
                        x1_ref = result['trackone'].loc[e1_label, columns].values.T
                        x1_ref[4, :] = 0  # MADX considers non-linear effects for path length difference (see above).
                        x1 = numpy_compatible(sbend.linear)(x0)
                        x1[4] = 0
                        self.assertTrue(e1_allclose(x1, x1_ref))

    def test_zero_angle(self):
        beam_config = {'particle': 'proton', 'beta': 0.6, 'gamma': 1.25}
        l = 5.
        x0 = backend.from_numpy(np.random.uniform(-0.001, 0.001, size=(6, 100)))
        self.assertTrue(F.all(SBend(0., l, beam=beam_config).linear(x0) == Drift(l, beam=beam_config).linear(x0)))

    def test_pole_face_rotation(self):
        beam = dict(particle='proton', beta=0.6, gamma=1.25)
        x0 = np.random.uniform(-0.001, 0.001, size=(6, 100))
        x0[[4, 5], :] = 0
        track = track_script(x0, [e1_label], aperture=False, recloss=False)
        for angle in np.random.uniform(0, np.pi/2, size=3):
            for sign in (-1, 1):
                angle *= sign
                for e1, e2 in np.random.uniform(0, angle/2, size=(3, 2)):
                    for fint in (1/6, 0.4, 0.7, 0.45, True):
                        for fintx in (fint, None):
                            if fintx is True:  # Setting fintx to True is not defined.
                                continue
                            hgap = np.random.uniform(0.1, 1)
                            with self.subTest(angle=angle, e1=e1, e2=e2, fint=fint, fintx=fintx, hgap=hgap):
                                sbend = SBend(angle, 5.0, e1, e2, fint, fintx, hgap, beam=beam, label='sbend_1')
                                sequence = sequence_script_from_list([sbend, Marker(label=e1_label)])
                                script = create_script(beam, sequence=sequence, track=track)
                                script = script.replace('refer = entry',
                                                        'refer = centre').replace('at = 0.0',
                                                                                  'at = 2.5')
                                script = script.replace(
                                    'track,',
                                    'select, flag = makethin, range = "sbend_1", makedipedge = true, thick = true;'
                                    '\nmakethin, sequence = seq;'
                                    '\nuse, sequence = seq;'
                                    '\ntrack,'
                                )
                                result = run_script(script, ['trackone'])
                                x1_ref = result['trackone'].loc[e1_label, columns].values.T
                                x1_ref[4, :] = 0
                                x1 = numpy_compatible(sbend.linear)(x0)
                                x1[4, :] = 0
                                self.assertTrue(e1_allclose(x1, x1_ref))


class TestMakethin(unittest.TestCase):
    @staticmethod
    def _compute_deltas(n: int):
        """Compute delta and Delta."""
        return (0.5 / (1 + n), n / (n**2 - 1)) if n > 1 else (0.5, 0)

    def test_drift(self):
        d = Drift(1.0, beam=dict(particle='proton', beta=0.6, gamma=1.25), label='test')
        thin = d.makethin(1)
        self.assertEqual(len(thin), 3)
        self.assertEqual(thin[0].l, 0.5)
        self.assertEqual(thin[1].l, 0.0)
        self.assertEqual(thin[2].l, 0.5)
        self.assertEqual(thin[0].label, 'test__d0')
        self.assertEqual(thin[1].label, 'test__0')
        self.assertEqual(thin[2].label, 'test__d1')

    def test_kickers(self):
        beam = dict(beta=0.6, gamma=1.25, particle='proton')
        x0 = np.random.uniform(-0.001, 0.001, size=(6, 100))
        track = track_script(x0, [e1_label], aperture=False, recloss=False)
        for n in range(1, 11):
            cls = Kicker
            with self.subTest(n=n, cls=cls):
                L, kick = 12.0, 0.0012
                kicker = cls(**dict(l=L, beam=beam, **{k: kick for k in cls.__annotations__}))
                segment = kicker.makethin(n, style='edge')
                self.assertSequenceEqual([type(x) for x in segment.elements],
                                         [Drift] + [cls, Drift] * (n-1) + [cls, Drift])
                d, D = (0, 1/(n-1)) if n >= 2 else (0.5, 0)
                self.assertSequenceEqual([e.l for e in segment.elements], [d*L] + [0., D*L] * (n-1) + [0., d*L])
                for val in [getattr(e, k) for e in segment.elements[1::2] for k in type(e).__annotations__]:
                    self.assertAlmostEqual(val, kick/n, delta=1e-12)
                self.assertSequenceEqual([e.label for e in segment.elements],
                                         [f'None__{d}{i}' for i in range(n+1) for d in ('d', '')][:-1])
                sequence = sequence_script_from_list([kicker, Marker(label=e1_label)])
                sequence = sequence.replace('entry', 'centre').replace('at = 0.0', f'at = {L/2}')
                script = create_script(beam, sequence=sequence, track=track)
                script = script.replace(
                    'track,',
                    f'select, flag = makethin, slice = {n}, class = "{cls.__name__.lower()}";\n'
                    f'makethin, sequence = "seq";\n'
                    f'use, sequence = "seq";\n'
                    f'track,'
                )
                result = run_script(script, ['trackone'])
                x1_ref = result['trackone'].loc[e1_label, columns].values.T
                x1 = numpy_compatible(segment.linear)(x0)
                self.assertTrue(e1_allclose(x1, x1_ref))

    def test_quadrupole(self):
        beam = dict(beta=0.6, gamma=1.25, particle='proton')
        x0 = np.random.uniform(-0.001, 0.001, size=(6, 100))
        track = track_script(x0, [e1_label], aperture=False, recloss=False)
        for n in range(1, 11):
            with self.subTest(n=n):
                L, k1 = 12.0, 0.0012
                quadrupole = Quadrupole(k1, L, beam=beam)
                segment = quadrupole.makethin(n)
                self.assertSequenceEqual(
                    [type(x) for x in segment.elements],
                    [Drift] + [ThinQuadrupole, Drift] * (n-1) + [ThinQuadrupole, Drift]
                )
                d, D = self._compute_deltas(n)
                self.assertSequenceEqual([e.l for e in segment.elements], [d*L] + [0., D*L] * (n-1) + [0., d*L])
                for val in [e.k1l for e in segment.elements if isinstance(e, ThinQuadrupole)]:
                    self.assertAlmostEqual(val.item(), k1*L/n, delta=1e-12)
                sequence = sequence_script_from_list([quadrupole, Marker(label=e1_label)])
                sequence = sequence.replace('entry', 'centre').replace('at = 0.0', f'at = {L/2}')
                script = create_script(beam, sequence=sequence, track=track)
                script = script.replace(
                    'track,',
                    f'select, flag = makethin, slice = {n}, thick = false, class = "quadrupole";\n'
                    f'makethin, sequence = "seq";\n'
                    f'use, sequence = "seq";\n'
                    f'track,'
                )
                result = run_script(script, ['trackone'])
                x1_ref = result['trackone'].loc[e1_label, columns].values.T
                x1 = numpy_compatible(segment.linear)(x0)
                self.assertTrue(e1_allclose(x1, x1_ref))
        q = Quadrupole(1, 2, beam=dict(beta=0.6, gamma=1.25))
        self.assertIs(q.makethin(0), q)

    def test_sbend(self):
        with self.assertRaises(NotImplementedError):
            SBend(0.1, 5.0, beam=dict(beta=0.6, gamma=1.25, particle='proton')).makethin(3)

    def test_create_thin_sequence(self):
        with self.assertRaises(ValueError):
            ThinElement.create_thin_sequence(0, 1, Drift, ThinQuadrupole, '', 'teapot')
        thin_cls = partial(HKicker, 0.1)
        thick_cls = partial(Drift, beam=dict(beta=0.6, gamma=1.25))
        for n in range(1, 11):
            self.assertEqual(len(ThinElement.create_thin_sequence(n, 1.0, thick_cls, thin_cls, '', 'teapot')), 2*n + 1)

    def test_wrapped(self):
        element = Tilt(Offset(Quadrupole(0.1, 5.0, beam=dict(beta=0.6, gamma=1.25)), 1.0), 2.0)
        thin = element.makethin(1)
        self.assertIsNot(thin, element)
        self.assertIsNot(thin.target, element.target)
        self.assertIsNot(thin.element, element.element)
        self.assertIsInstance(element.element, Quadrupole)
        self.assertIsInstance(thin, Tilt)
        self.assertIsInstance(thin.target, Offset)
        self.assertIsInstance(thin.target.target, Segment)
        self.assertIs(thin.element, thin.target.target)
        self.assertEqual(thin.psi, 2.0)
        self.assertEqual(thin.target.dx, 1.0)
        self.assertEqual(thin.target.dy, 0.0)
        self.assertSequenceEqual([type(x) for x in thin.element.elements], [Drift, ThinQuadrupole, Drift])

    def test_special_cases(self):
        q = Quadrupole(2.0, 1.0, beam=dict(beta=0.6, gamma=1.25), label='test')
        self.assertIs(q.makethin(0), q)
        q = Quadrupole(2.0, 0.999e-6, beam=dict(beta=0.6, gamma=1.25), label='test')
        self.assertIs(q.makethin(3), q)
        k = HKicker(2.0, l=0.987e-6, beam=dict(beta=0.6, gamma=1.25), label='test')
        self.assertIs(k.makethin(3), k)
        k.makethin_min_length = 1e-7
        self.assertIsInstance(k.makethin(3), Segment)


class TestThinQuadrupole(unittest.TestCase):
    def test_makethin(self):
        with self.assertRaises(NotImplementedError):
            ThinQuadrupole(1.0, label='test').makethin(0)
        with self.assertRaises(NotImplementedError):
            ThinQuadrupole(1.0, label='test').makethin(3)

    def test_track(self):
        x0 = np.random.uniform(-0.001, 0.001, size=(6, 100))
        track = track_script(x0, [e1_label], aperture=False, recloss=False)
        beam = dict(particle='proton', beta=0.6, gamma=1.25)
        quad = Quadrupole(0.01, 5.0, beam=beam, label='test')
        for n in range(2, 8):
            thin = quad.makethin(n)

            sequence_thin = sequence_script_from_list(thin.elements + [Marker(label=e1_label)])
            script_thin = create_script(beam, sequence=sequence_thin, track=track)
            result_thin = run_script(script_thin, ['trackone'])
            x1_thin = result_thin['trackone'].loc[e1_label, columns].values.T

            x1 = numpy_compatible(thin.linear)(x0)

            sequence_makethin = sequence_script_from_list([quad, Marker(label=e1_label)])
            sequence_makethin = sequence_makethin.replace('entry', 'centre').replace('at = 0.0', f'at = {quad.l / 2}')
            script_makethin = create_script(beam, sequence=sequence_makethin, track=track)
            script_makethin = script_makethin.replace(
                'track,',
                f'select, flag = makethin, slice = {n}, thick = false, class = "quadrupole";\n'
                f'makethin, sequence = "seq";\n'
                f'use, sequence = "seq";\n'
                f'track,'
            )
            result_makethin = run_script(script_makethin, ['trackone'])
            x1_ref = result_makethin['trackone'].loc[e1_label, columns].values.T

            self.assertTrue(e1_allclose(x1, x1_ref))
            self.assertTrue(e1_allclose(x1, x1_thin))
            self.assertTrue(e1_allclose(x1_thin, x1_ref))


class TestThinSextupole(unittest.TestCase):
    def test_makethin(self):
        with self.assertRaises(NotImplementedError):
            ThinSextupole(1.0, label='test').makethin(0)
        with self.assertRaises(NotImplementedError):
            ThinSextupole(1.0, label='test').makethin(3)


class TestAlignmentError(unittest.TestCase):
    def test_raise_attribute_error(self):
        a = AlignmentError(Quadrupole(2.0, 1.0, beam={'beta': 0.6, 'gamma': 1.25}, label='test'))
        with self.assertRaises(AttributeError):
            getattr(a, 'unknown_attr')
        with self.assertRaises(AttributeError):
            getattr(a, 'k1')

    def test_unwrap(self):
        a = Tilt(Offset(LongitudinalRoll(Marker(), 0.1), 0.2), 0.3)
        self.assertTrue(all(it.starmap(op.is_, it.zip_longest(a.unwrap(),
                                                              [a, a.target, a.target.target, a.target.target.target]))))

    def test_aperture_loss(self):
        x = backend.from_numpy(np.random.uniform(-0.05, 0.05, size=(6, 100)))
        d = Drift(1.0, beam=dict(beta=0.6, gamma=1.25), aperture=ApertureCircle(aperture=0.01))
        a = AlignmentError(d)
        self.assertTrue(np.array_equal(backend.to_numpy(a.loss(x)), backend.to_numpy(d.loss(x))))

    def test_enter_exit(self):
        a = AlignmentError(Quadrupole(2.0, 1.0, beam={'beta': 0.6, 'gamma': 1.25}, label='test'))
        x = np.random.random(size=(6, 100))
        self.assertTrue(np.array_equal(numpy_compatible(a.enter)(x), x))
        self.assertTrue(np.array_equal(numpy_compatible(a.exit)(x), x))


class TestOffset(unittest.TestCase):
    def test(self):
        beam_config = {'particle': 'proton', 'beta': 0.6, 'gamma': 1.25}
        elements = [Quadrupole(0.1, 2.0, beam=beam_config, label=e1_label)]
        x0 = np.random.uniform(-0.001, 0.001, size=(6, 100))
        x0[[4, 5], :] = 0
        track = track_script(x0, [e1_label], aperture=False, recloss=False)
        for dx, dy in np.random.uniform(-1e-2, 1e-2, size=(100, 2)):
            for element in elements:
                with self.subTest(dx=dx, dy=dy, element=element):
                    element = Offset(element, dx, dy)
                    script = create_script(beam_config, sequence=sequence_script_from_list([element]), track=track,
                                           errors=error_script_from_list([element]))
                    result = run_script(script, ['trackone'])
                    x1_ref = result['trackone'].loc[e1_label, columns].values.T
                    x1_ref[4, :] = 0  # MADX considers non-linear effects for path length difference (see above).
                    # Need to copy x0 because Offset .enter .exit modifies its argument in-place via slice assignment.
                    x1 = numpy_compatible(element.linear)(x0.copy())
                    self.assertTrue(e1_allclose(x1, x1_ref))


class TestLongitudinalRoll(unittest.TestCase):
    cls = LongitudinalRoll

    def test_rotation(self):
        xy_0 = np.zeros((6, 4), dtype=float)
        xy_0[0] = [1, 0, -1, 0]
        xy_0[2] = [0, -1, 0, 1]
        roll_clockwise = self.cls(Drift(0., beam={'beta': 0.6, 'gamma': 1.25}), np.pi / 2)
        xy_1 = numpy_compatible(roll_clockwise.enter)(xy_0)
        xy_1_ref = np.zeros((6, 4), dtype=float)
        xy_1_ref[0] = [0, -1, 0, 1]
        xy_1_ref[2] = [-1, 0, 1, 0]
        self.assertTrue(e1_allclose(xy_1, xy_1_ref))
        xy_2 = numpy_compatible(roll_clockwise.exit)(xy_1)
        self.assertTrue(e1_allclose(xy_2, xy_0))

        roll_ccw = self.cls(Drift(0., beam={'beta': 0.6, 'gamma': 1.25}), -np.pi / 2)
        xy_1 = numpy_compatible(roll_ccw.enter)(xy_0)
        xy_1_ref = np.zeros((6, 4), dtype=float)
        xy_1_ref[0] = [0, 1, 0, -1]
        xy_1_ref[2] = [1, 0, -1, 0]
        self.assertTrue(e1_allclose(xy_1, xy_1_ref))
        xy_2 = numpy_compatible(roll_ccw.exit)(xy_1)
        self.assertTrue(e1_allclose(xy_2, xy_0))

    def test(self):
        beam_config = {'particle': 'proton', 'beta': 0.6, 'gamma': 1.25}
        elements = [Quadrupole(0.1, 2.0, beam=beam_config, label=e1_label), HKicker(0.01, label=e1_label),
                    VKicker(0.1, label=e1_label)]
        x0 = np.random.uniform(-0.001, 0.001, size=(6, 100))
        x0[[4, 5], :] = 0
        track = track_script(x0, [e1_label], aperture=False, recloss=False)
        for psi in np.random.uniform(-np.pi, np.pi, size=25):
            for element in elements:
                with self.subTest(psi=psi, element=element):
                    sequence = []
                    if element.l == 0:  # Need to insert drift because MADX can't deal with zero-length sequences.
                        sequence.append(Drift(1.0, beam=beam_config))
                    element = self.cls(element, psi)
                    sequence.insert(0, element)
                    script = create_script(beam_config, sequence=sequence_script_from_list(sequence), track=track,
                                           errors=error_script_from_list([element]))
                    result = run_script(script, ['trackone'])
                    x1_ref = result['trackone'].loc[e1_label, columns].values.T
                    if isinstance(element.target, Quadrupole):
                        x1_ref[4, :] = 0  # MADX considers non-linear effects for path length difference (see above).
                    x1 = numpy_compatible(element.linear)(x0)
                    self.assertTrue(e1_allclose(x1, x1_ref))


class TestTilt(TestLongitudinalRoll):
    cls = Tilt


class TestBPMError(unittest.TestCase):
    def test(self):
        err = BPMError(Monitor(0.0, beam=dict(beta=0.6, gamma=1.25)), 0.1, 0.2, 0.3, 0.4)
        x0 = np.random.uniform(-0.001, 0.001, size=(6, 100))
        self.assertTrue(np.array_equal(
            numpy_compatible(err.readout)(x0),
            [[1.3], [1.4]] * (x0[[0, 2]] + [[0.1], [0.2]])
        ))

    def test_enter_exit(self):
        err = BPMError(Monitor(0.0, beam=dict(beta=0.6, gamma=1.25)), 0.1, 0.2, 0.3, 0.4)
        x0 = backend.from_numpy(np.random.uniform(-0.001, 0.001, size=(6, 100)))
        self.assertIs(err.enter(x0), x0)
        self.assertIs(err.exit(x0), x0)

    def test_noise_setattr(self):
        err = BPMError(Monitor(0.0, beam=dict(beta=0.6, gamma=1.25)))
        self.assertIsInstance(err.noise_scale, backend.TensorType)
        err = BPMError(Monitor(0.0, beam=dict(beta=0.6, gamma=1.25)), noise_scale=[1, 2])
        self.assertIsInstance(err.noise_scale, backend.TensorType)
        self.assertTupleEqual(err.noise_scale.shape, (2,))

    def test_noise_readout(self):
        err = BPMError(Monitor(0.0, beam=dict(beta=0.6, gamma=1.25)), noise_scale=[1, 2])
        x0 = backend.make_zeros(6, 100)
        x1 = err.readout(x0)
        self.assertGreater(x1.std(), 0)
        err.noise_scale = (1e-250, 1e-250)
        x2 = err.readout(x0)
        self.assertEqual(x2.std(), 0)


class TestSegment(unittest.TestCase):
    def test(self):
        s = Segment([HKicker(-0.5), HKicker(0.5)])
        x = np.random.uniform(-0.001, 0.001, size=(6, 100))
        self.assertTrue(e1_allclose(numpy_compatible(s.linear)(x), x))

    def test_flatten(self):
        beam = dict(beta=0.6, gamma=1.25)
        s = Segment([Quadrupole(2.0, 2.0, beam=beam, label='q1'), Segment([Quadrupole(4.0, 4.0, beam=beam, label='q2')])])
        f = s.flat()
        self.assertEqual(len(f.elements), 2)
        self.assertIs(f.elements[0], s.elements[0])
        self.assertIs(f.elements[1], s.elements[1].elements[0])
        f = s.makethin(2).flat()
        self.assertEqual(len(f.elements), 10)
        self.assertTrue(all(x.label.startswith('q1') for x in f.elements[:5]))
        self.assertTrue(all(x.label.startswith('q2') for x in f.elements[5:]))
        self.assertSequenceEqual([type(x) for x in f.elements],
                                 [Drift, ThinQuadrupole, Drift, ThinQuadrupole, Drift, Drift, ThinQuadrupole, Drift,
                                  ThinQuadrupole, Drift])

    def test_squeeze(self):
        beam = dict(beta=0.6, gamma=1.25)
        s_old = Segment([
            Drift(l=0, beam=beam, label='d1'),
            Quadrupole(2.0, 2.0, beam=beam, label='q1'),
            Drift(l=1, beam=beam, label='d2'),
            Drift(l=1, beam=beam, label='d3'),
            Drift(l=2, beam=beam, label='d4'),
            Monitor(l=0, beam=beam, label='m1'),
            Drift(l=4, beam=beam, label='d5'),
            Drift(l=4, beam=beam, label='d6'),
        ])
        s_new = s_old.squeeze()
        self.assertEqual(len(s_new), 5)
        self.assertIs(s_new[0], s_old[0])
        self.assertIs(s_new[1], s_old[1])
        self.assertIs(type(s_new[2]), Drift)
        self.assertEqual(s_new[2].label, 'd2_d3_d4')
        self.assertEqual(s_new[2].l, 4.0)
        self.assertIs(s_new[3], s_old[5])
        self.assertIs(type(s_new[4]), Drift)
        self.assertEqual(s_new[4].label, 'd5_d6')
        self.assertEqual(s_new[4].l, 8.0)

        s_new = s_old.squeeze(labeler=lambda x: 'test')
        self.assertEqual(s_new[2].label, 'test')
        self.assertEqual(s_new[4].label, 'test')

    def test_squeeze_compare_madx(self):
        script = importlib.resources.read_text('dipas.test.sequences', 'sis18.seq')
        script = re.sub('multipole.*?(?=;)', 'placeholder', script.lower())
        script = ''.join(script.partition('use, sequence=sis18lattice;')[:2])
        s_old = from_script(script)
        s_new = s_old.squeeze()
        with tempfile.TemporaryDirectory() as tmp_dir:
            tmp_file = os.path.join(tmp_dir, 'squeeze.seq')
            script += f'\nsave, sequence = SIS18LATTICE, file = "{tmp_file}";'
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                run_script(script)
            s_madx = from_file(tmp_file, beam=s_new[0].beam)
        self.assertEqual(len(s_new), len(s_madx))
        for e1, e2 in zip(s_new, s_madx):
            with self.subTest(e1=e1, e2=e2):
                self.assertIs(type(e1), type(e2))
                self.assertAlmostEqual(e1.l.item(), e2.l.item(), places=6)

    def test_makethin(self):
        s = Segment([Quadrupole(120.0, 24.0, beam=dict(beta=0.6, gamma=1.25), label='q_1'),
                     HKicker(240.0, l=36.0, beam=dict(beta=0.6, gamma=1.25), label='h_kicker_1')])
        thin_variants = [s.makethin(3), s.makethin({None: 3}),
                         s.makethin({Element: 3, Quadrupole: 5, HKicker: 7, re.compile('.+'): 9}),
                         s.makethin({re.compile('.+'): 3, re.compile('.*'): 5, Element: 7, Quadrupole: 9, None: 11})]
        for thin in thin_variants:
            self.assertSequenceEqual([type(x) for x in thin.elements], [ThinElement, ThinElement])
            self.assertSequenceEqual([type(x) for x in thin.elements[0].elements],
                                     [Drift, ThinQuadrupole, Drift, ThinQuadrupole, Drift, ThinQuadrupole, Drift])
            self.assertSequenceEqual([type(x) for x in thin.elements[1].elements],
                                     [Drift, HKicker, Drift, HKicker, Drift, HKicker, Drift])
            self.assertSequenceEqual([x.l for x in thin.elements[0].elements[::2]], [3.0, 9.0, 9.0, 3.0])
            self.assertSequenceEqual([x.k1l for x in thin.elements[0].elements[1::2]], [120 * 24 / 3] * 3)
            self.assertSequenceEqual([x.l for x in thin.elements[1].elements[::2]], [4.5, 13.5, 13.5, 4.5])
            self.assertSequenceEqual([x.kick for x in thin.elements[1].elements[1::2]], [240 / 3] * 3)

        thin_variants = [s.makethin({Quadrupole: 2, HKicker: 3, VKicker: 4, None: 5}),
                         s.makethin({Quadrupole: 2, re.compile('[a-z_]+_1'): 3, HKicker: 4}),
                         s.makethin({re.compile('q_1'): 2, re.compile('[a-z_]+_1'): 3, HKicker: 4})]
        for thin in thin_variants:
            self.assertSequenceEqual([type(x) for x in thin.elements], [ThinElement, ThinElement])
            self.assertSequenceEqual([type(x) for x in thin.elements[0].elements],
                                     [Drift, ThinQuadrupole, Drift, ThinQuadrupole, Drift])
            self.assertSequenceEqual([type(x) for x in thin.elements[1].elements],
                                     [Drift, HKicker, Drift, HKicker, Drift, HKicker, Drift])
            self.assertSequenceEqual([x.l for x in thin.elements[0].elements[::2]], [4.0, 16.0, 4.0])
            self.assertSequenceEqual([x.k1l for x in thin.elements[0].elements[1::2]], [120 * 24 / 2] * 2)
            self.assertSequenceEqual([x.l for x in thin.elements[1].elements[::2]], [4.5, 13.5, 13.5, 4.5])
            self.assertSequenceEqual([x.kick for x in thin.elements[1].elements[1::2]], [240 / 3] * 3)

        thin = s.makethin({Quadrupole: 2})
        self.assertIsInstance(thin.elements[0], ThinElement)
        self.assertIs(thin.elements[1], s.elements[1])

        with self.assertRaises(TypeError):
            s.makethin({1: 2})

    def test_raise_attribute_error(self):
        with self.assertRaises(AttributeError):
            getattr(Segment([Drift(1.0, beam=dict(beta=0.6, gamma=1.25))]), 'unknown_attr')

    # noinspection PyTypeChecker,PyStatementEffect
    def test_getitem(self):
        s = Segment([Drift(12.0, beam=dict(beta=0.6, gamma=1.25), label='drift_1'),
                     Quadrupole(120.0, 24.0, beam=dict(beta=0.6, gamma=1.25), label='q_1'),
                     Offset(Drift(16.0, beam=dict(beta=0.6, gamma=1.25), label='drift_2'), dx=5.0),
                     HKicker(240.0, l=36.0, beam=dict(beta=0.6, gamma=1.25), label='h_kicker_1')])
        cases = [
            (0, s.elements[0]),
            (1, s.elements[1]),
            (2, s.elements[2]),
            (3, s.elements[3]),
            (re.compile('[a-z]+_1'), s.elements[:2]),
            ((re.compile('[a-z]+_1'), 0), s.elements[0]),
            ((re.compile('[a-z]+_1'), 1), s.elements[1]),
            (re.compile('.+_1'), s.elements[:2] + s.elements[-1:]),
            ('*_1', s.elements[:2] + s.elements[-1:]),
            (re.compile('^[a-z]+$'), []),
            ('drift_2', s.elements[2]),
            (Drift, s.elements[::2]),
            ('drift_*', s.elements[::2]),
            (Offset, s.elements[2:3]),
            (Tilt, []),
            (Quadrupole, s.elements[1:2]),
            (HKicker, s.elements[-1:]),
            ((HKicker, 0), s.elements[-1]),
            (Kicker, s.elements[-1:]),
            (VKicker, []),
            (slice(None, None), s.elements),
            (slice(1, None), s.elements[1:]),
            (slice(None, -1), s.elements[:-1]),
            (slice(1, -1), s.elements[1:-1]),
            (slice('q_1', -1), s.elements[1:-1]),
            (slice(None, 'drift_2'), s.elements[:-1]),
            (slice(None, (Offset, 0)), s.elements[:-1]),
            (slice(None, s.elements[-2]), s.elements[:-1]),
            (slice(s.elements[1], s.elements[-2]), s.elements[1:-1]),
        ]
        for selection, expected in cases:
            with self.subTest(selection=selection, expected=expected):
                elements = s[selection]
                if isinstance(selection, slice):
                    self.assertIsInstance(elements, Segment)
                    elements = elements.elements
                elif isinstance(expected, list):
                    self.assertIsInstance(elements, list)
                else:
                    self.assertIsInstance(elements, (Element, AlignmentError))
                if not isinstance(expected, list):
                    expected = [expected]
                    elements = [elements]
                self.assertSequenceEqual(elements, expected)
                self.assertTrue(all(it.starmap(op.is_, it.zip_longest(elements, expected))))
        with self.assertRaises(IndexError):
            s['unknown':]
        with self.assertRaises(IndexError):
            s[1:'illegal']
        with self.assertRaises(TypeError):
            s[1.0]
        with self.assertRaises(TypeError):
            s[object()]

    def test_getitem_case_insensitive(self):
        beam = dict(beta=0.6, gamma=1.25)
        s = Segment([Drift(0, beam=beam, label='Drift_1'),
                     Drift(0, beam=beam, label='ThisIsDrift2'),
                     Drift(0, beam=beam, label='drift_3'),
                     Drift(0, beam=beam, label='this_is_drift_4')])
        self.assertSequenceEqual(s['Drift_*'], [s[0]])
        self.assertSequenceEqual(s['drift_*'], [s[2]])
        s.GETITEM_CASE_SENSITIVE = False
        self.assertIs(s['Drift_1'], s[0])
        self.assertIs(s['DRIFT_1'], s[0])
        self.assertIs(s['drift_1'], s[0])
        self.assertSequenceEqual(s['Drift_*'], [s[0], s[2]])
        self.assertSequenceEqual(s['drift_*'], [s[0], s[2]])
        self.assertIs(s['THIS_IS_DRIFT_4'], s[3])
        self.assertSequenceEqual(s['this*is*drift*'], [s[1], s[3]])

    # noinspection PyStatementEffect
    def test_getitem_element_not_found(self):
        s = Segment([Drift(0, beam=dict(beta=0.6, gamma=1.25), label='drift')])
        with self.assertRaises(ElementNotFoundError):
            s['Drift']
        self.assertIs(s['drift'], s[0])
        s.GETITEM_CASE_SENSITIVE = False
        with self.assertRaises(ElementNotFoundError):
            s['does_not_exist']

    def test_delitem(self):
        with importlib.resources.path('dipas.test.sequences', 'hades.seq') as path:
            with warnings.catch_warnings():
                warnings.simplefilter('ignore')
                lattice = from_file(path)
        length = len(lattice)
        n_drift = len(lattice[Drift])
        n_quad = len(lattice[Quadrupole])

        q_gte1qd11 = lattice['gte1qd11']
        del lattice['gte1qd11']  # Delete one quadrupole.
        self.assertEqual(len(lattice), length)
        self.assertEqual(len(lattice[Drift]), n_drift + 1)
        self.assertEqual(len(lattice[Quadrupole]), n_quad - 1)
        self.assertIsInstance(lattice['gte1qd11'], Drift)
        self.assertEqual(lattice['gte1qd11'].l, q_gte1qd11.l)
        self.assertEqual(lattice['gte1qd11'].beam, q_gte1qd11.beam)
        self.assertEqual(lattice['gte1qd11'].label, q_gte1qd11.label)

        q_gte2qtxx = lattice['gte2qt*']
        del lattice['gte2qt*']  # Delete a full triplet.
        d_gte2qtxx = lattice['gte2qt*']
        self.assertEqual(len(lattice), length)
        self.assertEqual(len(lattice[Drift]), n_drift + 4)
        self.assertEqual(len(lattice[Quadrupole]), n_quad - 4)
        self.assertEqual(len(q_gte2qtxx), len(d_gte2qtxx))
        for quad, drift in zip(q_gte2qtxx, d_gte2qtxx):
            self.assertIsInstance(drift, Drift)
            self.assertEqual(quad.l, drift.l)
            self.assertEqual(quad.beam, drift.beam)
            self.assertEqual(quad.label, drift.label)
        del lattice[Quadrupole]
        self.assertEqual(len(lattice), length)
        self.assertEqual(len(lattice[Quadrupole]), 0)
        self.assertEqual(len(lattice[Drift]), n_drift + n_quad)

    def test_insert(self):
        beam = dict(beta=0.6, gamma=1.25)
        s = Segment([Drift(12.0, beam=beam, label='drift_1'),
                     Quadrupole(120.0, 24.0, beam=beam, label='q_1'),
                     Offset(Drift(16.0, beam=beam, label='drift_2'), dx=5.0),
                     HKicker(240.0, l=36.0, beam=beam, label='h_kicker_1')])

        new_drift = Drift(0, beam=beam, label='new_drift')
        s.insert(0, new_drift)
        self.assertEqual(len(s), 5)
        self.assertIs(s[0], new_drift)
        self.assertIs(getattr(s, '_e0_new_drift'), new_drift)

        new_drift = Drift(0, beam=beam, label='new_drift')
        s.insert(4, new_drift)
        self.assertEqual(len(s), 6)
        self.assertIs(s[-2], new_drift)
        self.assertIs(getattr(s, '_e4_new_drift'), new_drift)

        new_drift = Drift(0, beam=beam, label='new_drift')
        s.insert(6, new_drift)
        self.assertEqual(len(s), 7)
        self.assertIs(s[-1], new_drift)
        self.assertIs(getattr(s, '_e6_new_drift'), new_drift)

    def test_forward(self):
        beam = dict(beta=0.6, gamma=1.25)
        x0 = np.zeros((6, 5), dtype=float)
        x0[0, :] = [0.0, 0.0  , 0.1, 0.007, 0.0   ]
        x0[1, :] = [0.0, 0.001, 0.0, 0.001, 0.0051]
        lattice = Segment([
            Drift(1.0, beam=beam, label='d1', aperture=ApertureCircle(0.01)),
            Drift(1.0, beam=beam, label='d2', aperture=ApertureCircle(0.01)),
            Drift(1.0, beam=beam, label='d3', aperture=ApertureEllipse((0.01, 0.05))),
            Drift(1.0, beam=beam, label='d4', aperture=ApertureRectangle((0.01, np.inf))),
            Drift(1.0, beam=beam, label='d5', aperture=ApertureCircle(0.01)),
        ])
        ret = lattice.exact(backend.from_numpy(x0))
        self.assertIsInstance(ret, backend.TensorType)
        self.assertTupleEqual(tuple(ret.shape), x0.shape)
        script = create_script(beam,
                               sequence=sequence_script_from_list(lattice.elements),
                               track=track_script(x0, ['d5'], aperture=False, recloss=False))
        result = run_script(script, ['trackone'])
        x1_ref = result['trackone'].loc['d5', columns].values.T
        self.assertTrue(e1_allclose(backend.to_numpy(ret), x1_ref))

        ret = lattice.exact(backend.from_numpy(x0), aperture=True)
        self.assertIsInstance(ret, backend.TensorType)
        self.assertTupleEqual(tuple(ret.shape), (6, 2))
        script = create_script(beam,
                               sequence=sequence_script_from_list(lattice.elements),
                               track=track_script(x0, ['d5'], aperture=True, recloss=False))
        result = run_script(script, ['trackone'])
        x1_ref = result['trackone'].loc['d5', columns].values.T
        self.assertTrue(e1_allclose(backend.to_numpy(ret), x1_ref))

        ret = lattice.exact(backend.from_numpy(x0), aperture=True, recloss='sum')
        self.assertIsInstance(ret, tuple)
        self.assertIsInstance(ret[0], backend.TensorType)
        self.assertIsInstance(ret[1], backend.TensorType)
        self.assertTupleEqual(tuple(ret[0].shape), (6, 2))
        self.assertTupleEqual(tuple(ret[1].shape), tuple())
        self.assertGreater(ret[1].item(), 0.)

        ret = lattice.exact(backend.from_numpy(x0), aperture=True, recloss=True)
        self.assertIsInstance(ret, tuple)
        self.assertIsInstance(ret[0], backend.TensorType)
        self.assertIsInstance(ret[1], dict)
        self.assertSequenceEqual(list(ret[1]), ['d1', 'd2', 'd3', 'd4', 'd5'])
        self.assertTupleEqual(tuple(ret[0].shape), (6, 2))
        self.assertSequenceEqual([tuple(x.shape) for x in ret[1].values()], [(5,), (4,), (4,), (3,), (2,)])

        ret = lattice.exact(backend.from_numpy(x0), aperture=True, recloss=[re.compile(r'd\d')],
                            loss_func=lambda x: backend.as_float64(x > 0))
        self.assertIsInstance(ret, tuple)
        self.assertIsInstance(ret[0], backend.TensorType)
        self.assertIsInstance(ret[1], dict)
        self.assertSequenceEqual(list(ret[1]), ['d1', 'd2', 'd3', 'd4', 'd5'])
        self.assertTupleEqual(tuple(ret[0].shape), (6, 2))
        self.assertSequenceEqual([tuple(x.shape) for x in ret[1].values()], [(5,), (4,), (4,), (3,), (2,)])
        self.assertSequenceEqual([backend.functions.sum(x).item() for x in ret[1].values()], [1.0, 0.0, 1.0, 1.0, 0.0])

        ret = lattice.exact(backend.from_numpy(x0), aperture=True, recloss='sum',
                            loss_func=lambda x: backend.as_float64(x > 0).sum())
        self.assertIsInstance(ret, tuple)
        self.assertIsInstance(ret[0], backend.TensorType)
        self.assertIsInstance(ret[1], backend.TensorType)
        self.assertTupleEqual(tuple(ret[0].shape), (6, 2))
        self.assertEqual(ret[1].item(), 3.0)

        ret = lattice.exact(backend.from_numpy(x0), observe=['d1', 'd2', 'd3', 'd4', 'd5'])
        self.assertIsInstance(ret, tuple)
        self.assertIsInstance(ret[0], backend.TensorType)
        self.assertIsInstance(ret[1], dict)
        self.assertTupleEqual(tuple(ret[0].shape), x0.shape)
        self.assertTrue(np.array_equal(backend.to_numpy(ret[0]), backend.to_numpy(ret[1]['d5'])))
        script = create_script(beam,
                               sequence=sequence_script_from_list(lattice.elements),
                               track=track_script(x0, ['d1', 'd2', 'd3', 'd4', 'd5'], aperture=False, recloss=False))
        result = run_script(script, ['trackone'])
        for place in ['d1', 'd2', 'd3', 'd4', 'd5']:
            self.assertTrue(e1_allclose(backend.to_numpy(ret[1][place]), result['trackone'].loc[place, columns].values.T))

    def test_forward_different_methods(self):
        beam = {'beta': 0.6, 'gamma': 1.25}
        lattice = Segment([
            Drift(l=1, beam=beam, label='drift_1'),
            Quadrupole(k1=0.1, l=2, beam=beam, label='quad_1'),
            Quadrupole(k1=0.1, l=2, beam=beam, label='quad_2'),
        ])
        x0 = backend.from_numpy(np.random.uniform(-0.01, 0.01, size=(6, 250)))
        expected = backend.to_numpy(lattice.elements[2].linear(lattice.elements[1].second_order(lattice.elements[0].linear(x0))))
        x1 = backend.to_numpy(lattice.forward(x0, method={Drift: 'linear',
                                                          'quad_2': Quadrupole.linear,
                                                          Quadrupole: Quadrupole.second_order}, exact_drift=False))
        x2 = backend.to_numpy(lattice.forward(x0, method={re.compile('drift_[0-9]'): Drift.linear,
                                                          re.compile('quad_[2-9]'): 'linear',
                                                          re.compile('quad_[0-9]'): 'second_order'}, exact_drift=False))
        self.assertTrue(np.array_equal(x1, expected))
        self.assertTrue(np.array_equal(x2, expected))
        expected = backend.to_numpy(lattice.linear(x0))
        x1 = lattice.forward(x0, method={None: 'linear'})
        x2 = lattice.forward(x0, method={'*_*': 'linear'})
        self.assertTrue(np.array_equal(x1, expected))
        self.assertTrue(np.array_equal(x2, expected))

    def test_forward_all_particles_lost_in_between(self):
        beam = {'beta': 0.6, 'gamma': 1.25}
        aperture = ApertureCircle(0.1)
        lattice = Segment([
            Drift(l=1, beam=beam, label='drift_1', aperture=aperture),
            Quadrupole(k1=0.1, l=2, beam=beam, label='quad_1', aperture=aperture),
            Quadrupole(k1=0.1, l=2, beam=beam, label='quad_2', aperture=aperture),
        ])
        x0 = backend.make_zeros(6, 1)
        x0[1, 0] = 0.2
        x1 = lattice.linear(x0, aperture=True)
        self.assertEqual(x1.shape, (6, 0))
        x1, history = lattice.linear(x0, observe=['*'], aperture=True)
        self.assertEqual(x1.shape, (6, 0))
        self.assertEqual(history['drift_1'].shape, (6, 1))
        self.assertEqual(history['quad_1'].shape, (6, 0))
        self.assertEqual(history['quad_2'].shape, (6, 0))


class TestUtilities(unittest.TestCase):
    def setUp(self):
        random.seed(12345)

    def test_symplectify(self):
        def allclose(x, y):
            if isinstance(x, backend.TensorType):
                x = backend.to_numpy(x)
            if isinstance(y, backend.TensorType):
                y = backend.to_numpy(y)
            return np.allclose(x, y, atol=1e-15, rtol=1e-14)

        with self.subTest('test identity'):
            R = backend.make_id_matrix(6)
            self.assertTrue(allclose(Utilities.symplectify(R), R))
        beam = Beam(particle='proton', gamma=1.25).to_dict()
        for name, cls in elements.items():
            attributes = {k: random.uniform(0.001, 1) for k in cls.get_attribute_names() if k not in cls.field_errors.values()}
            if issubclass(cls, (HKicker, VKicker)):
                del attributes['hkick']
                del attributes['vkick']
            if issubclass(cls, Marker):
                attributes['l'] = 0.
            element = cls(beam=beam, **attributes)
            R = element.R
            if issubclass(cls, CompoundElement):
                R = Utilities.symplectify(R)
            with self.subTest(f'test symplecticity condition ({cls})'):
                self.assertTrue(allclose((backend.transpose(R) @ Utilities.S @ R), Utilities.S))
            with self.subTest(f'test symplectifying again does not change the matrix ({cls})'):
                self.assertTrue(allclose(Utilities.symplectify(R), R))


class TestUseDriftFor(unittest.TestCase):
    original_elements: dict = None

    @classmethod
    def setUpClass(cls):
        cls.original_elements = elements.copy()

    @classmethod
    def tearDownClass(cls):
        elements.clear()
        elements.update(cls.original_elements)

    def test_single(self):
        with use_drift_for('quadrupole'):
            self.assertIsInstance(elements['quadrupole'], partial)
            self.assertDictEqual(elements['quadrupole'].keywords, {'l': 0})
        self.assertIs(elements['quadrupole'], Quadrupole)

    def test_multiple(self):
        with use_drift_for('quadrupole', 'hkicker'):
            self.assertIsInstance(elements['quadrupole'], partial)
            self.assertIsInstance(elements['hkicker'], partial)
        self.assertIs(elements['quadrupole'], Quadrupole)
        self.assertIs(elements['hkicker'], HKicker)

    def test_new_identifier(self):
        with use_drift_for('new_identifier'):
            self.assertIsInstance(elements['new_identifier'], partial)
        self.assertNotIn('new_identifier', elements)


if __name__ == '__main__':
    unittest.main()
